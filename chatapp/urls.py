from django.conf.urls import url, include
from django.urls import path
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.staticfiles.storage import staticfiles_storage

from . import views


urlpatterns = [
    path('jsi18n/', views.js_catalog_lang, name="javascript-catalog"),
    path('jsi18n/p', views.js_catalog_lang_p, name="javascript-catalog-p"),
    path('list/lang', views.get_lang_list),
    path('set/lang', views.set_lang),
    path('list/themes/', views.get_themes),
    path('list/themes/switch_dark', views.theme_dark_switch),
    path('list/themes/set_theme', views.set_theme),
    path('list/permissions/', views.get_permissions),
    path('signout/', views.signout, name='logout'),

    path('room_add_user/', views.room_add_user),
    path('room_rem_user/', views.room_rem_user),
    path('room_create/', views.chat_create),
    path('room_delete/', views.chat_delete),
    path('room_set_role/', views.room_set_role),
    path('room_del_message/', views.room_del_message),

    path('files/upload/', views.upload_files),

    path('mes_search/', views.mes_search),

    path('user_search/', views.user_search),

    url(r'^upload/',views.serve_uploads),

    url(r'^accounts/', include('django.contrib.auth.urls')),
    url(r'^list/themes/', views.get_themes, name='get_themes'),
    url(r'^login/', views.chat_login, name='login'),
    url(r'^chatjs/', views.chatjs, name='chatjs'),
    #url(r'^set/', views.chat_settings, name='chat_settings'),
    #url(r'^vocabs_mem/', views.vocabs_mem, name='vocabs_mem'),
    #url(r'^vocabs_room/', views.vocabs_room, name='vocabs_room'),
    path('', views.index, name='index'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) #+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
