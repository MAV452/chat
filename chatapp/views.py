# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import asyncio
import codecs
import datetime
import json
import os
from hashlib import md5
from urllib.parse import quote
import pathlib

import celery
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from chat.settings import STATIC_URL
from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.hashers import PBKDF2PasswordHasher, make_password
from django.contrib.auth.models import AnonymousUser, User
from django.contrib.auth.views import LoginView as loginform
from django.contrib.sessions.models import Session
from django.contrib.staticfiles.finders import find
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.middleware.csrf import get_token
from django.shortcuts import redirect, render
from django.utils import encoding
from django.utils.decorators import sync_and_async_middleware
from django.utils.translation import gettext_lazy as _
from django.views.decorators.csrf import csrf_exempt, ensure_csrf_cookie
from settings_local import BROKER_URL
from django.db.models import Q

from chatapp import classes, models

from . import classes as b_classes
from . import ext_models
from .decorators import ajax_login_required, s_id_from_params
from .models import ChatMembers, Room

ADMIN_ROLE_ID = 1

@s_id_from_params
@ajax_login_required
def theme_dark_switch(request):
    """
        Требует авторизации
        Переключает цветовую тему текущего пользователя со светлой на тёмную и наоборот

        Ошибки:
            Permission denied - Нет необходимых прав

        при успешном выполнении возвращает
        {
            'success':True
        }
    """
    user=classes.get_user(request)
    user_id = -1
    if not(user is None):
        user_id=user.id
    if not(b_classes.is_allowed(user.id,'themes')):
        return JsonResponse({'error': _("Permission denied")})

    settings = models.pers_settings.get_settings(user_id)
    settings.Theme_dark = not(settings.Theme_dark)
    settings.save()
    return JsonResponse({'success':True})

def get_lang_list(request):
    """
        Возвращает список поддерживаемых языков

        {
            'list':[
                {
                    'code': код языка
                    'name': название языка
                    'cur': язык выбран в данный момент
                },
                ...
            ]
        }
    """
    out = []
    user = classes.get_user(request)
    user_id = -1
    if not(user is None):
        user_id = user.id
    cur = b_classes.get_lang(user_id)
    for item in settings.LANGUAGES:
        out.append({'code':item[0],'name':item[1],'cur':item[0]==cur})
    return JsonResponse({'list':out})

@s_id_from_params
def set_lang(request):
    """
        Изменяет язык пользователя
        GET-параметры:
            lang - код языка

        Ошибки:
            Lang error - нет языка с таким кодом
        
        При успешном выполнении возвращает:
        {
            'success':True
        }
    """
    user=classes.get_user(request)
    if (not(user.is_authenticated)):
        #return JsonResponse({'error':'Auth error'})    
        return JsonResponse({'no_auth':1})
    lang = request.GET.get('lang','')
    l_l = []
    for l in settings.LANGUAGES:
        l_l.append(l[0])
    if not(lang in l_l):
        return JsonResponse({'error':'Lang error'})
    set_o = models.pers_settings.get_settings(user.id)
    set_o.Lang = lang
    set_o.save()
    return JsonResponse({'success':True})

@s_id_from_params
def get_permissions(request):
    """
        Возвращает названия функций, на которые у текущего пользователя есть разрешение

        {
            'list':[
                'file_attach',
                ...
            ]
        }
    """
    #client = ext_models.client_user.objects.filter(auth_id=request.user.id, is_active=True).first()
    #if client is None:
    #    return ({'error': _("Auth error")})
    out=[]
    user=classes.get_user(request)
    out = b_classes.get_permissions(user)
    return JsonResponse({'list':out})

@s_id_from_params
def js_catalog_lang(request):
    """
        Возвращает файл JavaScriptCatalog для языка указанного в настройках пользователя,
        если не удаётся, берёт язык из настроек проекта, если также не удаётся, устанавливает в 'en'
    """
    user=classes.get_user(request)
    lang=b_classes.get_lang(user.id)
    return HttpResponse(b_classes.JavaScriptCatalogSwitch.as_view(domain='django', lang=lang)(request), content_type="text/javascript")

def js_catalog_lang_p(request):
    """
        Возвращает файл JavaScriptCatalog для языка указанного в GET-параметре lang
        если параметр не указан, берёт язык из настроек проекта
    """
    lang=request.GET.get('lang',settings.LANGUAGE_CODE)
    return HttpResponse(b_classes.JavaScriptCatalogSwitch.as_view(domain='django', lang=lang)(request), content_type="text/javascript")

def chat_members_list():
    """
        Возвращает массив объектов кто в каком чате состоит
        в следующем виде:
        [
            {
                'chat_id': ИД чата,
                'user_id': ИД пользователя (в Django auth)
            }
        ]
    """
    res = ChatMembers.objects.all()
    out = []
    for item in res:
        out.append ({'chat_id': item.chat.id, 'user_id': item.user.id})
    return out

def get_chat_list():
    """
        Возвращает массив чатов вследующем виде:
        [
            {
                'id': ИД чата,
                'name' Название чата:
            },
            ...
        ]
    """
    res = Room.objects.filter(is_active = True)
    out = []
    for item in res:
        out.append ({'id': item.id, 'name': item.title})
    return out

def vocabs_mem(request):
    """
        Возврашает необходимые словари для страницы управления доступом к чатам
    """
    out = {}
    out['rooms'] = get_chat_list()
    out['members'] = chat_members_list()
    return JsonResponse(out, safe=False)

def vocabs_room(request):
    """
        Возврашает необходимые словари для страницы управления чатами
    """
    out = {}
    out['rooms'] = get_chat_list()
    return JsonResponse(out, safe=False)

def chat_settings(request):
    """
        Выполняет команды настройки чата, передаваемые как ajax-запросы
        Данные передаются как GET-параметры

        Параметр 'obj' определяет с каким типом объектов
        будет выполняться действие. Может принимать значения:
        'mem' - доступ пользователей к чатам
        'list' - список чатов

        Параметр 'com' определяет какое действие будет выполняться.
        Возможные варианты зависят от параметра 'obj'
        Если его значение 'mem':
            'add': лишить пользователся доступа к определённому чату
            'del': дать пользователю доступ к определённому чату
        'list':
            'del': удалить чат
            'ren': переименовать чат
            'cre': создать чат

        Также передаются параметры:
        Для obj = 'mem':
            user_id: ИД пользователя
            room_id: ИД чата
        Для obj = 'list':
            room_id: ИД чата
            room_name: Название чата (при переименовании 
            и создании чата этот параметр будет установлен как имя)
    """
    if (request.GET['obj'] == 'mem'):
        uid = request.GET['user_id']
        rid = request.GET['room_id']
        obj = ChatMembers.objects.filter(chat_id = rid, chat__is_active=1, user_id = uid)
        if (request.GET['com'] == 'add'):
            if (obj.count() == 0):
                ChatMembers.objects.create(chat_id = rid, chat__is_active=1, user_id = uid)
        if (request.GET['com'] == 'del'):
            if (obj.count() > 0):
                obj.delete()

    elif (request.GET['obj'] == 'list'):
        rid = request.GET['room_id']
        rname = request.GET['room_name']
        if (request.GET['com'] == 'del'):
            rooms = Room.objects.filter(id = rid, is_active=1,)
            if (rooms.count() > 0):
                room = rooms[0]
                room.is_active = False
                room.save()
        if (request.GET['com'] == 'ren'):
            if (Room.objects.filter(title = rname, is_active=1,).count() == 0 and len(rname) > 0):
                rooms = Room.objects.filter(id = rid, is_active=1,)
                if (rooms.count() > 0):
                    room = rooms[0]
                    room.title = rname
                    room.save()

        if (request.GET['com'] == 'cre'):
            rooms = Room.objects.filter(title = rname)
            if (rooms.count() == 0):
                Room.objects.create(title = rname)
            else:
                room = rooms[0]
                room.is_active = True
                room.save()
    
    return JsonResponse([], safe=False)


def chat_login(request):
    """
    Осуществляет авторизацию в чат

    При включённой сихронизации с БД СУРа:
        Проверяет есть ли пользователь с таким логином в базе СУРа
        Если пользователь есть и пароль подходит дублирует пользоателя в базу чата
        Также проверяет совпадение паролей в СУРе и чате, при несовпадении пароль СУРа переносится на чат
        Считается что в СУРе и в чате используется стандартный алгоритм шифрования pbkdf2_sha256, если в пароле используется
        другой алгоритм выдаёт ошибку

    Использует для авторизации модель ChatAuth, заменяющую стандартные сессии для исключения конфликтов при использовании
    сессий других приложений
    
    Стандартная сессия при авторизации остаётся неавторизованной, но её ИД используется в ChatAuth

    POST-параметры
    login - логин пользователя
    pwd - пароль пользователя

    Ошибки:
        username param error - не удалось получить логин
        password param error - не удалось получить пароль
        

    возвращает
        {
            'success': в зависимости от успешности авторизации True или False
        }

    """
    uname = request.POST.get('login',None)
    if (uname is None):
        return JsonResponse({'error':'username param error'})
    pwd = request.POST.get('pwd',None)
    if (pwd is None):
        return JsonResponse({'error':'password param error'})
    ext_user = None
    uis = models.UserIsSur.objects.filter(user__username=uname).first()
    if (settings.SUR_AUTH):
        if not(uis is None):
            ext_user = ext_models.ext_user.objects.filter(id=uis.s_u_id).first()
        else:
            ext_user = ext_models.ext_user.objects.filter(username=uname).first()
    iter = 10000
    alg = ''
    salt = None
    if not(ext_user is None):
        salt = ext_user.password.split('$')
        if (len(salt)>=3):
            alg = salt[0]
            iter = salt[1]
            salt = salt[2]
        else:
            salt=None
    pwd_h = ''
    if (settings.SUR_AUTH):
        pwd_h = classes.password_hash(pwd,salt, iter, alg)

    if (uis is None):
        user = User.objects.filter(username=uname).first()
    else:
        user = uis.user
    if (ext_user is None):#Если не нашлось пользователя СУР
        if (user is None):#Если не нашлось пользователя чата
            return JsonResponse({'success':False})
    else:
        if(ext_user.password!=pwd_h):#Проверка пароля СУР
            return JsonResponse({'success':False})
        elif(user is None):#Если нет пользователя чата создаём и авторизуем
            user = User.objects.create_user(uname,ext_user.email,pwd)
            user.is_superuser=ext_user.is_superuser
            user.is_staff=ext_user.is_staff
            user.is_active=ext_user.is_active
            user.save()
            uis = models.UserIsSur(user=user, s_u_id=ext_user.id)
            uis.save()
            classes.login_user(request, user)
            return JsonResponse({'success':True})
        elif(user.password!=ext_user.password):#если пароли пользователей СУРа и чата не совпадает, меняем пароль чата и авторизуем
            if not(uis is None)and(ext_user.id==uis.s_u_id):#но сначала проверяем что пользователь был перенесён из СУРа
                user.password=ext_user.password
                user.save()
                classes.login_user(request, user)
                return JsonResponse({'success':True})
            else:
                return JsonResponse({'success':False})
    if (user is None):#Если не нашлось пользователя чата
        return JsonResponse({'success':False})
    iter = 10000
    alg = ''
    salt = user.password.split('$')
    if (len(salt)>=3):
        alg = salt[0]
        iter = salt[1]
        salt = salt[2]
    else:
        salt=None
    u_pwd_h = classes.password_hash(pwd,salt,iter,alg)
    if (user.password==u_pwd_h):
        classes.login_user(request, user)
        return JsonResponse({'success':True})
    return JsonResponse({'success':False})

def chat_update(room_id):
    layer = get_channel_layer()
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    task = loop.create_task(
        layer.group_send(
            models.Room.websocket_group_stb_by_id(room_id),
            {'type':'websocket.send', 'text':json.dumps({'chat_update':1})}
        )
    )
    loop.run_until_complete(task)

@s_id_from_params
@ajax_login_required
def room_add_user(request):
    """
        добавляет пользователя в комнату
        post-параметры:
        room - ИД комнаты
        u_id - ИД пользователя

        Ошибки:
            Access denied - у пользователя нет доступа к комнате
            Permission denied - Нет необходимых прав
            No such user - нет пользователя с таким ИД
            Already added - пользователь уже находится в комнате
            Room error - нет комнаты с таким ИД

        при успешном выполнении возвращает
        {
            'success':True
        }

        и рассылает через websocket всем пользователям состоящим в комнате:
        {
            'chat_update':1 //команда на обновление списка комнат
        }

        требует авторизаци, принадлежности пользователя к комнате чата и наличия соответствующих прав в комнате
    """
    user = classes.get_user(request)
    room_id = request.POST.get('room',-1)
    perm_id = models.ChatMembers.objects.filter(user_id=user.id, chat__is_active=1,chat__id=room_id).values_list('role',flat=True)
    if len(perm_id)==0:
        return JsonResponse({'error': "Access denied"})
    perm_id=perm_id[0]
    if not(models.ChatMembers.is_permited(perm_id,'add_users')):
        return JsonResponse({'error': "Permission denied"})
    add_user_name = request.POST.get('u_name',"")
    add_user_obj = User.objects.filter(username=add_user_name).first()
    if (add_user_obj is None):
        return JsonResponse({'error': _("No such user")})
    if models.ChatMembers.objects.filter(user_id=add_user_obj.id, chat__is_active=1,chat__id=room_id).exists():
        return JsonResponse({'error': _("Already added")})
    room_obj = models.Room.objects.filter(id = room_id, is_active=1,).first()
    if (room_obj is None):
        return JsonResponse({'error': _("Room error")})
    mem_obj = models.ChatMembers(user_id=add_user_obj.id,chat=room_obj)
    mem_obj.save()

    chat_update(room_id)
    
    return JsonResponse({'success':True})

@s_id_from_params
@ajax_login_required
def room_rem_user(request):
    """
        удаляет пользователя из комнаты
        post-параметры:
        room - ИД комнаты
        u_id - ИД пользователя

        Ошибки:
            Access denied - у пользователя нет доступа к комнате
            Permission denied - Нет необходимых прав
            No such user - нет пользователя с таким ИД
            User is not in the room - у пользователя нет доступа к комнате
            Can't remove the only admin - нельзя удалить единственного админа комнаты

        при успешном выполнении возвращает
        {
            'success':True
        }

        и рассылает через websocket всем пользователям состоящим в комнате:
        {
            'chat_update':1 //команда на обновление списка комнат
        }

        требует авторизаци, принадлежности пользователя к комнате чата и наличия соответствующих прав в комнате
        если пользователь удаляет сам себя особые права не требуются
    """
    user = classes.get_user(request)
    room_id = request.POST.get('room',-1)
    perm_id = models.ChatMembers.objects.filter(user_id=user.id, chat__is_active=1,chat__id=room_id).values_list('role',flat=True)
    if len(perm_id)==0:
        return JsonResponse({'error': "Access denied"})
    perm_id=perm_id[0]
    mem_room_q = models.ChatMembers.objects.filter(chat__id=room_id, chat__is_active=1,)
    add_user_id = request.POST.get('u_id',-1)
    if (str(add_user_id)!=str(user.id)):
        if not(models.ChatMembers.is_permited(perm_id,'add_users')):
            return JsonResponse({'error': "Permission denied"})
    add_user_obj = User.objects.filter(id=add_user_id).first()
    if (add_user_obj is None):
        return JsonResponse({'error': _("No such user")})
    admin_count = mem_room_q.filter(role=ADMIN_ROLE_ID).count()
    mem_obj = mem_room_q.filter(user_id=add_user_obj.id).first()
    if (mem_obj is None):
        return JsonResponse({'error': _("User is not in the room")})
    if (admin_count<=1 and mem_obj.role==ADMIN_ROLE_ID):
        return JsonResponse({'error': _("Can't remove the only admin")})
    mem_obj.delete()

    chat_update(room_id)
    
    return JsonResponse({'success':True})

@s_id_from_params
@ajax_login_required
def room_del_message(request):
    """
        помечает сообщение как удалённое или восстанавливает его в зависимости от значения параметра cmd
        post-параметры:
        m_id - ИД сообщения
        cmd - устанавливаемое значение, 0 - удалить 1 - восстановить

        Ошибки:
            Command error - не удалось получить параметр cmd
            Message error - Не удалось найти сообщение с таким ИД
            Access denied - у пользователя нет доступа к комнате
            Permission denied - Нет необходимых прав

        при успешном выполнении возвращает
        {
            'success':True
        }

        и рассылает через websocket всем пользователям состоящим в комнате:
        {
            'deleted':ИД сообщения
            'room':ИД комнаты
            'cmd':параметр cmd
        }

        требует авторизации, принадлежности пользователя к комнате чата и наличия соответствующих прав в ней
    """
    user = classes.get_user(request)
    mes_id = request.POST.get('m_id',-1)
    cmd = request.POST.get('cmd',-1)
    if (cmd == -1):
        return JsonResponse({'error': "Command error"})
    mes_obj = models.ChatMessage.objects.filter(id=mes_id).prefetch_related('room').first()
    if mes_obj is None:
        return JsonResponse({'error': "Message error"})
    room_obj = mes_obj.room
    perm_id = models.ChatMembers.objects.filter(user_id=user.id, chat__is_active=1,chat__id=room_obj.id).values_list('role',flat=True)
    if len(perm_id)==0:
        return JsonResponse({'error': "Access denied"})
    perm_id=perm_id[0]
    if (str(mes_obj.sender_id)!=str(user.id)):
        if not(models.ChatMembers.is_permited(perm_id,'del_message')):
            return JsonResponse({'error': "Permission denied"})
    else:
        if not(models.ChatMembers.is_permited(perm_id,'del_message_self')):
            return JsonResponse({'error': "Permission denied"})

    mes_obj.is_active=cmd
    mes_obj.save()

    layer = get_channel_layer()

    
    async_to_sync(layer.group_send)(
            room_obj.websocket_group_stb,
            {'type':'websocket.send', 'text':json.dumps(
                {
                    'deleted':str(mes_id),
                    'room':str(mes_obj.room.id),
                    'cmd':str(cmd),
                })}
        )

    return JsonResponse({'success':True})

@s_id_from_params
@ajax_login_required
def room_set_role(request):
    """
    Задаёт роль пользователя в комнате чата
        Получает на вход POST-параметры:
            room - ИД комнаты,
            u_id - ИД пользователя,
            role - ИД роли,

        Ошибки:
            Access denied - нет комнаты с таким ИД или у пользователя нет к ней доступа
            Permission denied - нет необходимых прав
            No such user - нет пользователя с таким ИД
            Param error - нет необходимых параметров (role)
            User is not in the room - у пользователя нет доступа к комнате
            Can't remove the only admin - нельзя изменить роль единственного админа комнаты

        при успешном выполнении возвращает
        {
            'success':True
        }

        требует авторизации, принадлежности пользователя к комнате чата и наличия соответствующих прав в ней
    """
    user = classes.get_user(request)
    room_id = request.POST.get('room',-1)
    perm_id = models.ChatMembers.objects.filter(user_id=user.id, chat__is_active=1,chat__id=room_id).values_list('role',flat=True)
    if len(perm_id)==0:
        return JsonResponse({'error': "Access denied"})
    perm_id=perm_id[0]
    if not(models.ChatMembers.is_permited(perm_id,'set_roles')):
        return JsonResponse({'error': "Permission denied"})
    mem_room_q = models.ChatMembers.objects.filter(chat__id=room_id, chat__is_active=1,)
    set_user_id = request.POST.get('u_id',-1)
    set_user_obj = User.objects.filter(id=set_user_id).first()
    if (set_user_obj is None):
        return JsonResponse({'error': _("No such user")})
    role = request.POST.get('role',-1)
    if (role==-1):
        return JsonResponse({'error': "Param error"})
    admin_count = mem_room_q.filter(role=ADMIN_ROLE_ID).count()
    mem_obj = mem_room_q.filter(user_id=set_user_obj.id).first()
    if (mem_obj is None):
        return JsonResponse({'error': _("User is not in the room")})
    if (admin_count<=1 and mem_obj.role==ADMIN_ROLE_ID):
        return JsonResponse({'error': _("Can't remove the only admin")})
    mem_obj.role=role
    mem_obj.save()
    return JsonResponse({'success':True})

@s_id_from_params
@ajax_login_required
def chat_create(request):
    """
    Создаёт комнату, добавляет в неё текущего пользователя и назначает его админом
    Получает на вход POST-параметры:
        title - Название комнаты

    Ошибки:
        Can't fetch max length - не удалось получить максимальную длинну названия комнаты (проблема на сервере)
        Permission denied - нет необходимых прав
        Name error - не удалось получить параметр названия чата
        Max name length exceeded - превышена максимальная длина названия комнаты
        Name taken - Чат с таким именем уже существует (удаление чата не освобождает его имя)

        при успешном выполнении возвращает
        {
            'success':True
        }

        требует авторизации, принадлежности пользователя к комнате чата и наличия соответствующих прав в ней
    """
    name_max_len=models.Room._meta.get_field('title').max_length
    if (not name_max_len>0):
        return JsonResponse({'error': _("Can't fetch max length")})
    user = classes.get_user(request)
    if not(b_classes.is_allowed(user.id,'chat_create')):
        return JsonResponse({'error': _("Permission denied")})
    room_name = request.POST.get('name',None)
    if (room_name is None):
        return JsonResponse({'error': _("Name error")})
    if (len(room_name)>name_max_len):
        return JsonResponse({'error': _("Max name length exceeded")})
    if models.Room.objects.filter(title=room_name).exists():
        return JsonResponse({'error': _("Name taken")})
    room_obj = models.Room(title=room_name)
    room_obj.save()
    chat_mem_obj = models.ChatMembers(user_id=user.id,chat=room_obj, role=ADMIN_ROLE_ID)
    chat_mem_obj.save()

    #chat_update(room_obj.id)

    return JsonResponse({'success':True})

@s_id_from_params
@ajax_login_required
def chat_delete(request):
    """
        Удаляет комнату чата
        Получает на вход POST-параметр room - ИД комнаты.

        Ошибки:
            Access denied - нет комнаты с таким ИД или у пользователя нет к ней доступа
            Permission denied - нет необходимых прав
            Room error - комната уже удалена

        при успешном выполнении возвращает
        {
            'success':True
        }

        и рассылает через websocket всем пользователям состоящим в комнате:
        {
            'chat_update':1 //команда на обновление списка комнат
        }

        требует авторизации, принадлежности пользователя к комнате чата и наличия соответствующих прав в ней
    """
    user = classes.get_user(request)
    room_id = request.POST.get('room',None)
    perm_id = models.ChatMembers.objects.filter(user_id=user.id, chat__is_active=1,chat_id=room_id).values_list('role',flat=True)
    if len(perm_id)==0:
        return JsonResponse({'error': "Access denied"})
    perm_id=perm_id[0]
    if not(models.ChatMembers.is_permited(perm_id,'del_room')):
        return JsonResponse({'error': "Permission denied"})

    room_obj = models.Room.objects.filter(id = room_id, is_active=1,).first()
    if (room_obj is None):
        return JsonResponse({'error': _("Room error")})

    room_obj.is_active=0
    room_obj.save()

    chat_update(room_id)

    return JsonResponse({'success':True})

@s_id_from_params
@ajax_login_required
def user_search(request):
    """
    Поиск пользователей по ФИО без учёта регистра
    Получает на вход POST-параметр name - строку по которой будет осуществляться поиск.
    Работает только при включённой авторизации через СУР, иначе возвращает пустой массив.

    Возвращаемые данные имеют следующий формат:
    {
        'list':[
            {
                'id':ИД client_user или sentry_user,
                'c_u':client_user если 1, sentry user если 0
                'auth_id':ИД пользователя auth_user,
                'name':логин,
                'full_name':ФИО,
            },
            ...
        ]
    }

    Требует авторизации
    """

    #user = classes.get_user(request)

    out = []

    name = request.POST.get('name',None)
    if not(name is None)and len(name)>0:
        if (settings.SUR_AUTH):
            users = list(ext_models.client_user.objects.filter(full_name__icontains=name))
            for item in users:
                item.c_u=1
            users += list(ext_models.sentry_user.objects.filter(full_name__icontains=name))
            for usr in users:
                auth_id = models.UserIsSur.objects.filter(s_u_id=usr.auth_id).values_list('user_id', flat=True)
                if (len(auth_id)>0):
                    auth_user = User.objects.filter(id=auth_id[0]).first()
                    if (not auth_user is None):
                        out.append({
                            'id':usr.id,
                            'c_u':int(hasattr(usr,'c_u')),
                            'auth_id':auth_id[0],
                            'name':auth_user.username,
                            'full_name':usr.full_name,
                            #'source':'SUR'
                        })
        else:
            out = []
            #users = User.filter(name=name)

    return JsonResponse({'list':out})

@s_id_from_params
@ajax_login_required
def mes_search(request):
    """
    Поиск по сообщениям
    Получает на вход POST-параметры:
    Обязательные:
        room_id - ИД комнаты в которой будет осуществляться поиск
    Параметры поиска, могут отсутствовать
        date_from - отправленные после указанной даты
        date_to - отправленные до указанной даты
        text - текст, входящий в сообщение, без учёта регистра
        user - ИД пользователя auth_user, отправившего сообщение
    Если не указан ни один параметр поиска, возвращает пустой массив

    Возвращаемый результат:
    {
        'list':[
            {
                'id': ИД сообщения,
                'userID': ИД отправителя,
                'username': Имя отправителя,
                'attachment': количество приложений,
                'message': текст сообщения,
                'date': дата отправки сообщения в виде строки формата ГГГГ-М-Д ЧЧ:ММ:СС"
            },
            ...
        ]
    }

    Ошибки:
        Room error - не получен параметр ИД комнаты
        Access denied - нет комнаты с таким ИД или у пользователя нет к ней доступа
    
    требует авторизации и принадлежности пользователя к комнате чата
    """
    user = classes.get_user(request)

    #флаг показывающтй был ли применён хотя бы один фильтр что бы не вернуть просто все сообщения чата
    filtered = False

    r_id = request.POST.get('room_id',None)
    if (r_id is None):
        return JsonResponse({'error': _("Room error")})

    if not(models.ChatMembers.objects.filter(user_id=user.id, chat_id=r_id).exists()):
        return JsonResponse({'error': _("Access denied")})

    query = models.ChatMessage.objects.filter(room_id=r_id, is_active=1).order_by('date')

    d_f = request.POST.get('date_from',None)
    if not(d_f is None):
        filtered = True
        d_f = classes.datestr_to_datetime(d_f)
        query = query.filter(date__gte=d_f)

    d_t = request.POST.get('date_to',None)
    if not(d_t is None):
        filtered = True
        d_t = classes.datestr_to_datetime(d_t)
        d_t = d_t.replace(hour = 23, minute=59, second=59)
        query = query.filter(date__lte=d_t)

    text = request.POST.get('text',None)
    if not(text is None):
        filtered=True
        query = query.filter(message__icontains=text)

    user_id = request.POST.get('user',None)
    if not(user_id is None):
        filtered=True
        query = query.filter(sender_id=user_id)

    if (filtered):
        #query = list(query.values_list('id',flat=True))
        query = list(query)
        query_1 = []
        for item in query:
            query_1.append(item.to_dict_light)
        query = query_1

    else:
        query = []

    return JsonResponse({'list':query})

@s_id_from_params
@ajax_login_required
def set_theme(request):
    """
        Требует авторизации
        Задаёт текущему пользователю цветовую тему
        GET-параметры: 
            theme_id - ИД цветовой темы

        Ошибки:
            Permission denied - Нет необходимых прав
            Theme error - не найдена тема с таким ИД

        при успешном выполнении возвращает
        {
            'success':True
        }
    """
    user = classes.get_user(request)
    #if (not user.is_authenticated):
    #    return JsonResponse({'error': _("Auth error")})
    if not(b_classes.is_allowed(user.id,'themes')):
        return JsonResponse({'error': "Permission denied"})
    theme_id = request.GET.get('theme_id',-1)
    theme = models.color_themes.objects.filter(id=theme_id).first()
    if (theme is None):
        return JsonResponse({'error': "Theme error"})
    settings = models.pers_settings.get_settings(user.id)
    settings.Theme = theme
    settings.save()
    return JsonResponse({'success':True})

@s_id_from_params
def get_themes(request):
    """
        Возвращает список доступных текущему пользователю цветовых тем

        GET-параметры:
            cur - при наличии параметра будет возвращена только текущая тема, значение параметра не имеет значения

        Ошибки:
            Permission denied - отсутствуют необходимые права

        Возвращаемый список имеет следющий формат:
        {
            'list':[
                {
                    'id':ИД темы,
                    'name':Название темы,
                    'light':{цвета светлого варианта темы
                        'кодовое название цвета':{
                            'name':название цвета,
                            'value':код цвета вида #ffffff
                        },
                        ...
                    },
                    'dark':{цвета тёмного варианта темы
                        'кодовое название цвета':{
                            'name':название цвета,
                            'value':код цвета вида #ffffff
                        },
                        ...
                    },
                    'cur':использует ли текущий пользователь эту темы сейчас
                },
                ...
            ],
            'dark':в данный момент используется тёмная тема
        }
    """
    user=classes.get_user(request)
    if not(b_classes.is_allowed(user.id,'themes')):
        return JsonResponse({'error': "Permission denied"})
    cur = request.GET.get('cur', None)
    return JsonResponse(b_classes.get_themes(user.id,(not cur is None)))

@s_id_from_params
def upload_files(request):
    """
        Сохраняет приложения к сообщению и возвращает их ИД
        POST-параметры:
            room - ИД комнаты чата в которую отправляется сообщение
        Также в запросе находится FILES - Прилагаемые файлы

        Ошибки:
            auth error - ошибка авторизации
            Permission denied - отсутствие нобходимых прав
            room error - нет комнаты с таким ИД

        Возвращаемые данные:
        {
            'list':[
                ИД приложения,
                ...
            ]
        }
        Требует принадлежности пользователя к комнате и наличия у него соответствующих прав
    """
    out=[]
    user = classes.get_user(request)
    if (user is None):
        return JsonResponse({'error':'auth error'})
    if not(b_classes.is_allowed(user.id,'file_attach')):
        return JsonResponse({'error': "Permission denied"})
    room_id = request.POST.get('room',-1)
    room_m = models.ChatMembers.objects.filter(chat_id=room_id,chat__is_active=1,user_id=user.id).first()
    if (room_m is None):
        return JsonResponse({'error':'room error'})
    for file in request.FILES:
        a_obj = models.Attachment(room=room_m.chat, file=request.FILES[file])
        a_obj.save()
        out.append(a_obj.id)

    #models.Attachment.gen_thumb_all()
    
    return JsonResponse({'list':out})


def serve_uploads(request):
    """
    """
    media_root_count = 1 #количество папок от корня проекта до корня медиа
    file = request.path
    path = pathlib.Path(file)
    path = pathlib.Path(*path.parts[1+media_root_count:])
    if not(models.Attachment.objects.filter((Q(file=path) | Q(preview=path)) & Q(is_active=1)).exists()):
        return HttpResponse(status=404)
    response = HttpResponse(status=200)
    response['Content-Type'] = ''
    response["X-Accel-Redirect"] = '/protected'+ quote(file)
    return response

def chatjs(request):
    """
        Берёт из статики файл основного скрипта, и подставляет в начало строковую переменную
        chaturl - URL сервера чата
    """
    js_path = "chatapp/vjs/chatapp.js"
    js_path_1 = "chatapp/chat_init.js"
    js_path_2 = "chatapp/vjs/store.js"
    js_path_3 = "vendors.js"
    abs_path = find(js_path)
    abs_path_1 = find(js_path_1)
    abs_path_2 = find(js_path_2)
    abs_path_3 = find(js_path_3)
    jscode = open(abs_path, 'r')
    jscode_1 = open(abs_path_1, 'r')
    jscode_2 = open(abs_path_2, 'r')
    jscode_3 = open(abs_path_3, 'r')
    lines = jscode.read()
    lines_1 = jscode_1.read()
    lines_2 = jscode_2.read()
    lines_3 = jscode_3.read()
    host = os.environ.get('HOST_URL', request.get_host())
    out = "var chaturl = \"" + host + "\";\n"
    s_id = request.COOKIES.get(settings.SESSION_COOKIE_NAME)
    if s_id is None:
        request.session.create()
        out += "var sessionid = \"" + request.session.session_key + "\";\n"
    title = _("WMS Account")
    user=classes.get_user(request)
    user_id = -1
    if (not user is None):
        user_id=user.id
    context = {
        'title':title,
        'version':settings.VERSION,
        'use_sur':int(settings.SUR_AUTH),
        'u_name':classes.get_u_name(user),
        'theme':json.dumps(classes.get_themes(user_id,True)),
    }
    out += "var chat_context_obj = " + str(context) + ";\n"
    out+=lines_2
    out+=lines_3
    out+=lines_1
    out+=lines
    response = HttpResponse(bytes(out, 'utf-8'), content_type='text/javascript')
    return(response)

def index(request):
    """
    Root page view. This is essentially a single-page app, if you ignore the
    login and admin parts.
    """
    
    return b_classes.get_index(request, "chatapp/index.html")

@s_id_from_params
def signout(request):
    """
    Выход из текущей сессии чата
    При наличии GET-параметра 'all' выходит из всех сессий этого пользователя
    """
    #logout(request)
    session = request.COOKIES.get(settings.SESSION_COOKIE_NAME,None)
    ca_o = models.ChatAuth.objects.filter(key=session)
    ca_o_l = ca_o.last()
    if (request.GET.get('all',False)):
        if (not(ca_o_l is None)):
            ca_o1 = models.ChatAuth.objects.filter(Q(uid = ca_o_l.uid)|Q(uid_s=ca_o_l.uid_s))
            ca_o1.delete()
    else:
        ca_o.delete()
    return HttpResponse('')

# Create your views here.
