# -*- coding: utf-8 -*-
from django.conf import settings
from urllib.parse import urlparse

import logging
from django.middleware.csrf import CsrfViewMiddleware, _get_failure_view

from settings_local import SUR_HOSTS

class corsMiddleware(object):
    """
        Добавляет заголовки CORS (нужны для возврата ajax ответов)
        если запрос приходит с сайта указанного в SUR_HOSTS
    """
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        referer_p = urlparse(request.META.get('HTTP_REFERER',''))
        referer = referer_p.netloc
        if (referer in settings.SUR_HOSTS or settings.CORS_ALLOW_ALL_ORIGIN):
            response["Access-Control-Allow-Origin"] = referer_p.scheme+'://'+referer_p.netloc
            response["Access-Control-Allow-Headers"] = ','.join(settings.CORS_HEADERS)
            response["Access-Control-Allow-Credentials"] = 'true'

        return response

class xframeMiddleware(object):
    """
        Добавляет заголовок X-Frame-Option с значением "DENY"
        запрещающий встраивание сайта в i-frame
        если запрос приходит с сайта не указанного в SUR_HOSTS
    """
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        referer = urlparse(request.META.get('HTTP_REFERER','')).netloc
        if not(referer in settings.SUR_HOSTS):
            response["X-Frame-Option"] = "DENY"
        return response

class CustomCsrfMiddleware(CsrfViewMiddleware):
    """
        CSRF работает на куках, которые при встраивании можно задавать только по https,
        вместо этого проводится проверка что адрес запроса находится в списке разрешённых для встраивания
        и только потом при неудаче происходит нормальная проверка CSRF
    """

    def process_view(self, request, callback, callback_args, callback_kwargs):
        referer = urlparse(request.META.get('HTTP_REFERER','')).netloc
        if (referer in settings.SUR_HOSTS):
            return self._accept(request)
        else:
            return super().process_view(request, callback, callback_args, callback_kwargs)

