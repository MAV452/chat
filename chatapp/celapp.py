# -*- coding: utf-8 -*-
#chatapp/celapp.py
import os
from celery import Celery
from settings_local import BROKER_URL, CELERY_RESULT_BACKEND

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'chat.settings')

app = Celery('chatapp',  broker=BROKER_URL, backend=CELERY_RESULT_BACKEND)
app.config_from_object('django.conf:settings')
app.autodiscover_tasks()
