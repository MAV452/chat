from django.conf import settings
from django.contrib.auth import models
from django.core.management.base import BaseCommand
from django.core.management import call_command
from shutil import copyfile
import os
from chatapp.ext_models import ext_user
from django.contrib.auth.models import User
from chatapp import models

class Command(BaseCommand):
    """
        Перенос пользователя из базы СУРа по логину
        Если в базе СУРа нет пользователя с таким логином или пользователь
        с таким логином уже есть в базе чата то ничего не делает
    """
    help = "Copy user from SUR database by username"

    def add_arguments(self, parser):
        parser.add_argument('-u', type=str, help='Username from SUR database')

    def handle(self, *args, **options):
        if not(settings.SUR_AUTH):
            return
        
        username = options['u']

        if User.objects.filter(username=username).exists():
            return

        e_user = ext_user.objects.filter(username=username).first()
        if (e_user is None):
            return

        user = User.objects.create_user(
            username=e_user.username,
            password=e_user.password,
            email=e_user.email,
            is_superuser=e_user.is_superuser,
            is_staff=e_user.is_staff,
            is_active=e_user.is_active,
            )
        user.save()

        uis = models.UserIsSur(user=user, s_u_id=e_user.id)
        uis.save()



        