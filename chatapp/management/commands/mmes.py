from django.conf import settings
from django.core.management.base import BaseCommand
from django.core.management import call_command
from shutil import copyfile
import os

class Command(BaseCommand):
    """
        Выполняет Makemessages для доменов django и djangojs
        а затем собирает созданные ими файлы в один файл django.po без дублирования одинаковых строк.

        Файл tmp является копией django.po до выполнения функции на случай ошибок в её работе.
    """
    help = "Do Makemessages for django and djangojs domains and then combine them in one file."

    def handle(self, *args, **options):
        if not(os.path.exists(settings.BASE_DIR+'/locale')):
            os.mkdir(settings.BASE_DIR+'/locale/')
        for dir in settings.LANGUAGES:
            try:
                os.mkdir(os.path.join((settings.BASE_DIR+'/locale/'),dir[0]))
            except:
                pass
        #    call_command('makemessages',
        #        locale=llist,
        #        domain='django',
        #        extensions=['py', 'vue', 'js'],
        #        ignore_patterns=['node_modules*', 'external*', 'static*','admin/*'])

        for dir in os.listdir(settings.BASE_DIR+'/locale'):
            bd = settings.BASE_DIR+'/locale/'+dir+'/LC_MESSAGES/'
            try:
                os.rename(bd+'django.po', bd+'tmp.po')
            except:
                pass

        call_command('makemessages',
            all=True,
            domain='django',
            extensions=['py', 'vue', 'js'],
            ignore_patterns=['node_modules*', 'external*', 'static*','admin/*'])
        
        call_command('makemessages',
            all=True,
            domain='djangojs',
            extensions=['vue', 'js'],
            ignore_patterns=['node_modules*', 'external*', 'static*','admin/*'])
        for dir in os.listdir(settings.BASE_DIR+'/locale'):
            bd = settings.BASE_DIR+'/locale/'+dir+'/LC_MESSAGES/'
            try:
                f=open(bd+'tmp.po','r')
            except:
                copyfile(bd+'django.po', bd+'tmp.po')
                f=open(bd+'tmp.po','r')
            f1=open(bd+'django.po','r')
            f2=open(bd+'djangojs.po','r')
            tl=f.readlines()
            dl=f1.readlines()
            jl=f2.readlines()
            f.close()
            f1.close()
            f2.close()

            for l in [dl, jl]:
                ps=0
                for i in range(len(l)):
                    line = l[i]
                    if len(line)<=1:
                        ps=i
                    if line[0:5]=='msgid':
                        if not(line in tl):
                            tl.append('')
                            for j in range(ps,i+2):
                                tl.append(l[j])

            f=open(bd+'django.po','w')
            f1=open(bd+'tmp.po','w')
            for line in tl:
                f.write(line)
                f1.write(line)
            f.close()
            f1.close()

            os.remove(bd+'djangojs.po')



        