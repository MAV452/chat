from django.core.management.base import BaseCommand
from importlib import import_module

class Command(BaseCommand):
    """
        Назначает заново стандартные цветовые схемы по данным из 0002_set_theme_items.py
    """
    help = "Resets standaert themes listed in migraton file 0002 in base."
    migr = import_module('chatapp.migrations.0002_set_theme_items')

    def handle(self, *args, **options):
        self.migr.add_items()



        