from django.core.management.base import BaseCommand
from importlib import import_module

class Command(BaseCommand):
    """
        Добавляет новые группы и назнвчает заново их разрешения по данным из 0016_set_groups.py
    """
    help = "Resets permissions on groups and create new group if not exists yet"
    migr = import_module('chatapp.migrations.0016_set_groups')

    def handle(self, *args, **options):
        self.migr.add_items()



        