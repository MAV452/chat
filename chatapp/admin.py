# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Room, ChatMembers
from django.contrib.auth.models import User
from django import forms
# Register your models here.

class DontLog:
    def log_addition(self, *args):
        return

    def log_deletion(self, *args):
        return

    def log_change(self, *args):
        return

class ChatMembers_form(forms.ModelForm):
    users=User.objects.all()
    try:
        i = len(users)
    except:
        users=[]
    CHOICES = []
    for item in users:
        CHOICES.append(
            (item.id,item.username)
        )

    user_id = forms.ChoiceField(choices=CHOICES, label='Пользователь auth_user')

class RoomAdmin(DontLog, admin.ModelAdmin):
    fields=("title", "staff_only","is_active")
    list_display=("id", "title","staff_only","is_active")

admin.site.register(
    Room, RoomAdmin
)

def auth_username(obj):
    username=''
    user = User.objects.filter(id=obj).first()
    if not (user is None):
        username = user.username
    return username

class ChatMembersAdmin(DontLog, admin.ModelAdmin):
    fields=("user_id","chat","role")
    list_display=("user_id",'chat',"role")
    form=ChatMembers_form

admin.site.register(
    ChatMembers, ChatMembersAdmin,
)
