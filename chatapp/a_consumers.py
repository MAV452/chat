# -*- coding: utf-8 -*-
import json, psycopg2, datetime, redis
from django.core.checks import messages
from django.db.models.query import prefetch_related_objects
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
from channels.consumer import SyncConsumer
import channels, math
from chatapp import classes, models, ext_models
from django.contrib.auth.models import User
from django.conf import settings

from urllib.parse import parse_qs

from . import settings as c_set
from .utils import get_room_or_error, catch_client_error, get_user_obj

from . import classes as cl

ARCHIVE_SIZE = 10
ONLINE_TIMEOUT = 120
layer = get_channel_layer()

def read(mess, user, channel):
    """
        Отмечает факт загрузки сообщения mess пользователем user.
        Если сообщение новее последнего просмотренного, отправляет
        соответствующее уведомление всем окнам этого пользователя где открыт чат
        что бы убрать в них уведомление о новых сообщениях.
    """
    if(models.Unread.readed(mess, user)):
        count = models.Unread.getcount(mess.room,user)
        final_msg = {'room': str(mess.room.id), 'count':str(count), 'date':str(mess.date_str), 'user':str(user.id), 'msg_type': c_set.MSG_TYPE_READED}
        async_to_sync(layer.group_send)(mess.room.websocket_group_stb, {'type':'websocket.send','text':json.dumps(final_msg)})

def rd_online(channel,con = NotImplemented):
    """
    """
    if (con == NotImplemented):
        con = classes.get_redis_con()
    name = settings.REDIS_PREFIX+'chat_user_'+str(channel.user.id)+'|'+channel.channel_name
    con.set(name,'1')
    con.expire(name, ONLINE_TIMEOUT)

def rd_offline(channel,con = NotImplemented):
    """
    """
    if (con == NotImplemented):
        con = classes.get_redis_con()
    keys = con.keys(settings.REDIS_PREFIX+'chat_user_'+str(channel.user.id)+'|'+channel.channel_name)
    for key in keys:
        con.delete(key)

def rd_check_online(user_id, con=NotImplemented):
    """
    """
    if (con == NotImplemented):
        con = classes.get_redis_con()
    return len(con.keys(settings.REDIS_PREFIX+'chat_user_'+str(user_id)+'|*'))>0
    
def rd_join(channel, room, con = NotImplemented):
    """
    """
    if (con == NotImplemented):
        con = classes.get_redis_con()
    name = settings.REDIS_PREFIX+'chat_user_'+str(channel.user.id)+'^'+str(room)+'|'+channel.channel_name
    con.set(name,'1')
    con.expire(name, ONLINE_TIMEOUT)

def rd_leave(channel, room, con = NotImplemented):
    """
    """
    if (con == NotImplemented):
        con = classes.get_redis_con()
    keys = con.keys(settings.REDIS_PREFIX+'chat_user_'+str(channel.user.id)+'^'+str(room)+'|'+channel.channel_name)
    for key in keys:
        con.delete(key)

def rd_check_join(user_id, room_id, con=NotImplemented):
    """
    """
    if (con == NotImplemented):
        con = classes.get_redis_con()
    return len(con.keys(settings.REDIS_PREFIX+'chat_user_'+str(user_id)+'^'+str(room_id)+'|*'))>0

def getmessagesrd(room, con = NotImplemented):
    """
    берёт из ключей редиса все доступные сообщения для чата room
    в параметр con предполагается передавать редис-объект если к моменту
    вызова уже имеется созданный, если он не был передан, функция создаст свой
    """
    return
    if (con == NotImplemented):
        con = classes.get_redis_con()
    keys = con.keys(settings.REDIS_PREFIX+'chatmess:'+room+'|*>*')
    js=[]
    for key in keys:
        js.append(json.dumps({
            'room':room,
            'text':(con.get(key)),
            'userid':key[(key.find('|')+1):key.find('>')],
            'date':key[(key.find('>')+1):len(key)],
        }))
    return js

def getmessagespg(date, room, amount, dirrection="up"):
    """
    Берёт сообщения для отдельного чата из базы
    Берётся определённое количество сообщений начиная с определённой даты
    dirrection - "up"/'down'/"middle" брать сообщения раньше(по умолчанию)/позже/с обоих сторон от определённой даты
    """
    #celeryobj = celery.Celery(broker=BROKER_URL, backend=BROKER_URL)
    #res = celeryobj.send_task("chatapp.tasks.getmessage", kwargs={'chatid' : room, 'time' : date, 'amount' : amount})
    #return serializers.deserialize("json",res.get())
    if (room is None or date is None):
        return []
    if (date is str):
        date_1 = datetime.datetime.strftime(date,"%Y-%m-%d %H:%M:%S")
    else:
        date_1 = date
    if(dirrection=="up"):
        if (date_1 is str):
            date_1 = date_1 + datetime.timedelta(0,1)
        messages = list(models.ChatMessage.objects.order_by("-date").filter(room_id = room, date__lte = date_1, is_active = True).prefetch_related('reply')[:amount])
    elif (dirrection=="down"):
        if (date_1 is str):
            date_1 = date_1 - datetime.timedelta(0,1)
        messages = list(models.ChatMessage.objects.order_by("date").filter(room_id = room, date__gte = date_1, is_active = True).prefetch_related('reply')[:amount])
        messages.reverse()
    elif(dirrection=='newest'):
        messages = list(models.ChatMessage.objects.order_by("-date").filter(room_id = room, is_active = True).prefetch_related('reply')[:amount])
    elif (dirrection=='middle'):
        l1 = math.floor(amount/2)+1
        l2 = math.ceil(amount/2)
        messages_a = list(models.ChatMessage.objects.order_by("-date").filter(room_id = room, date__lte = date, is_active = True).prefetch_related('reply')[:l1])
        #messages_p = models.ChatMessage.objects.order_by("-date").filter(room_id = room, date__gte = date, is_active = True).prefetch_related('reply').reverse()[:l1]
        #messages_a = models.ChatMessage.objects.order_by("date").filter(room_id = room, date__lte = date, is_active = True).prefetch_related('reply').reverse()[:l2]
        messages_p = list(models.ChatMessage.objects.order_by("date").filter(room_id = room, date__gte = date, is_active = True).prefetch_related('reply')[:l2])
        messages_p.reverse()
        messages = []
        for item in messages_p:
            messages.append(item)
        for item in messages_a[1:]:
            messages.append(item)
    else:
        messages = []
    #print("---------")
    #print(dirrection)
    #print(date)
    #print("---------")
    #for item in messages:
    #    print(item.date, item.id)
    return messages

def isallowed(user_id, room):
    """
        Проверяет, что пользователю разрешён доступ в чат
    """
    obj = models.ChatMembers.objects.filter(user_id = user_id, chat_id = room.id, chat__is_active=1).count()
    if (obj>0):
        return True
    else:
        return False    

class ChatConsumer(WebsocketConsumer):

    def websocket_connect(self, event):
        """
            Открытие Websocket-соединения

            Данные авторизации берутся из сесии.
            Авторизация ищется в следующем порядке:
                - попытка взять пользователя из объекта request полученного из запроса на подключение
                - попытка получить ИД сессии из cookies и по нему определить пользователя через объект ChatAuth (дублирует сессии джанго,
                позволяет авторизироваться по ИД сессий других приложений не ломая сессию для этих приложений)
                - попытка получить ИД сессии из GET-параметра s_id (при невозможности получить его из cookies)

            Если авторизация не была найдена, отправляет websocket-сообщение 
            {
                'bad_auth':1
            }
            после чего закрывает канал.

            При удачной авторизации рассылает всем пользователям находящимся онлайн в данный момент websocket-сообщение 
            {
                'online': ИД пользователя
            }
            После этого отправляет через websocket список комнат пользователя и информацию о текущем пользователе
        """
        self.rooms = []
        user = self.scope['user']
        if not(user.is_authenticated):
            session = self.scope['cookies'].get(settings.SESSION_COOKIE_NAME)
            if (session is None):#если в куках сессии нет, ищем в get-параметрах
                query_params = parse_qs(self.scope["query_string"].decode())
                session = query_params.get('s_id',[''])[0]
            user = classes.check_chatauth(session)
        if (user is None):
            self.accept()
            self.send(json.dumps({
                'bad_auth':1,
            }))
            self.close()
            return
        else:
            self.user = user
        #self.channel_name = 'u'+str(self.user.id)
        self.accept()
        async_to_sync(layer.group_add)(
            'online',
            self.channel_name,
        )
        async_to_sync(layer.group_send)('online', {'type':'websocket.send', 'text':json.dumps({'online':user.id})})
        rd_online(self)
        async_to_sync(layer.group_add)(
            'u_'+str(self.user.id),
            self.channel_name,
        )
        query_params = parse_qs(self.scope["query_string"].decode())
        r_id = query_params.get('r_id',[''])[0]
        print(r_id)
        if (len(r_id)>0):
            chat_join(self,{'room':r_id})
        chat_get_u_info(self,event)
        chat_list(self,event)

    def websocket_disconnect(self, close_code):
        """
            Закрытие Websocket-соединения

            Рассылает всем пользователям находящимся онлайн в данный момент websocket-сообщение 
            {
                'offline': ИД пользователя
            }
            После чего закрывает канал
        """
        try:
            rd_offline(self)
        except:
            pass
        # Leave room group
        for r_id in self.rooms:
            room = get_room_or_error(r_id,self.user)
            async_to_sync(layer.group_discard)(
                room.websocket_group,
                self.channel_name
            )
            async_to_sync(layer.group_discard)(
                'online',
                self.channel_name
            )
            if not(rd_check_online(self.user.id)):
                async_to_sync(layer.group_send)('online', {'type':'websocket.send', 'text':json.dumps({'offline':self.user.id})})
            async_to_sync(layer.group_discard)(
            'u_'+str(self.user.id),
            self.channel_name,
        )

    def websocket_receive(self, event):
        payload = json.loads(event['text'])
        command_process(self,payload)

    def websocket_send(self, event):
        self.send((event['text']))

def command_process(channel,data):
    command = data.get('command','')
    room_id = data.get('room',None)
    if not(room_id  is None):#если указана комната, проверяется что она не удалена и к ней разрешён доступ, если нет - посылает команду на выход
        user = channel.user
        if not(models.ChatMembers.objects.filter(chat_id=room_id, chat__is_active=1,user_id=user.id).exists()):
            chat_leave(channel,data)
            async_to_sync(layer.group_discard)(
                models.Room.websocket_group_stb_by_id(room_id),
                channel.channel_name
            )
            chat_list(channel,data)
            return
    chat_pong(channel,data)
    print(command)
    if (command=='join'):
        chat_join(channel,data)
    elif (command=='ljoin'):
        chat_leavejoin(channel,data)
    elif (command=='getarchive'):
        chat_getarchive(channel,data)
    elif (command=='leave'):
        chat_leave(channel,data)
    elif (command=='send'):
        chat_send(channel,data)
    elif (command=='getuinfo'):
        chat_get_u_info(channel,data)
    elif (command=='read'):
        chat_read(channel,data)
    elif (command=='pong'):
        chat_pong(channel,data)
    elif (command=='chatlist'):
        chat_list(channel,data)

def chat_pong(channel,data):
    """
        Обновляет ключи статуса пользователя.
        Если во входных параметрах есть room (ИД комнаты) - обновляет ключи и для этой комнаты
    """
    rd_online(channel)
    room_id = data.get('room',None)
    if not(room_id is None):
        rd_join(channel,room_id)

def chat_read(channel,data):
    user = channel.user
    m_id = data.get('message',-1)
    mes = models.ChatMessage.objects.filter(id=m_id).first()
    if (not mes is None):
        read(mes, user, channel)

def chat_get_u_info(channel,data):
    user_id = data.get('u_id',-1)
    if (user_id==-1):
        user = channel.user
    else:
        user = User.objects.filter(id=user_id).first()
        if user is None:
            channel.send(json.dumps({
                "user_info": 1,
                "error":'user not found',
            }))
    user_full_name = cl.get_u_name(user.id)
    channel.send(json.dumps({
        "user_info": 1,
        "id":user.id,
        "full_name":user_full_name,
    }))

def chat_del_message(channel,data):
    """
    """
    user = channel.user

    m_id=data.get('m_id',-1)

    m_obj = models.ChatMessage.objects.filter(id=m_id).first()
    #if (m_obj is None):
    #    return JsonResponse({'error': "Room error"})

    room_id = m_obj.room.id

    perm_id = models.ChatMembers.objects.filter(user_id=user.id, chat__is_active=1,chat__id=room_id).values_list('role',flat=True)
    #if len(perm_id)==0:
    #    return JsonResponse({'error': "Access denied"})
    perm_id=perm_id[0]
    #if not(models.ChatMembers.is_permited(perm_id,'del_message')):
    #    return JsonResponse({'error': "Permission denied"})



def chat_send(channel,data):
    timest = datetime.datetime.now() #текущие время и дата в UTC

    edit = data.get('edit',-1)
    user = channel.user
    room = get_room_or_error(data["room"], user)
    user_full_name = cl.get_u_name(user.id)

    #r = redis.StrictRedis(host=settings.loccon.REDIS_HOST, port=settings.loccon.REDIS_PORT)
    #name = 'chatmess:' + data['room'] + '|' + str(user.id) + '>' + timest #добавление сообщения в редис с именем чата, пользователя и датой в качестве ключа
    #r.set(name, data['message'])
    #r.expire(name, settings.loccon.REDIS_MESSAGE_EXPIRE_TIME)


    #вызов задания на добавление сообщения в базу
    sender = get_user_obj(channel.user.id)
    roomobj = get_room_or_error(room.id, sender)
    reply = data.get('reply',-1)
    rep_mes = models.ChatMessage.objects.filter(id=reply).first()
    if (rep_mes is None):
        channel.send(json.dumps({
            "error": "Reply error",
        }))
    if (edit!=-1):
        mes = models.ChatMessage.objects.filter(id=edit, sender_id=user.id).first()
        if (mes is None):
            channel.send(json.dumps({
                "error": "Edit error",
            }))
        else:
            mes.reply = rep_mes
            mes.message = data.get('message','')
            mes.edited= 1
    else:
        mes = models.ChatMessage.objects.create(sender_id = sender.id, reply = rep_mes, room = roomobj, date = timest, message = data.get('message',''))
    mes.save()
    attachment = data.get('attachment',[])
    m_att = mes.attachment.all()
    for a_id in attachment:
        if not(m_att.filter(id=a_id).exists()):
            a_obj = models.Attachment.objects.filter(id=a_id).first()
            if (not(a_obj is None))and(roomobj.id==a_obj.room_id):
                mes.attachment.add(a_obj)
    if (m_att.count()!=len(attachment)):
        for att in m_att:
            if not(att.id in attachment):
                mes.attachment.remove(att)

    room.send_message(channel, mes)

    if (edit==-1):
        room.new_message(channel)
    #read(mes, sender, channel)
    #celeryobj = celery.Celery(broker=BROKER_URL, backend=BROKER_URL)
    #res = celeryobj.send_task("chatapp.tasks.send", kwargs={'message':message["message"],'user':message.user.id, 'room': room.id, 'time': timest})

def chat_join(channel,data):
    timest = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")#текущие время и дата в UTC
    user = channel.user
    user_full_name = cl.get_u_name(user.id)
    #room = get_room_or_error(message["room"], user)
    room = models.Room.objects.filter(id=data['room'], is_active=1,).first()
    rd_join(channel,room.id)
    if not(isallowed(user.id, room)):
        channel.send(json.dumps({
            "error": "Access denied",
        }))
        return
    #if NOTIFY_USERS_ON_ENTER_OR_LEAVE_ROOMS: #уведомление о входе и выходе пользовтелей
    #    room.send_message(None, user, user_full_name, timest, c_set.MSG_TYPE_ENTER)

    #room.websocket_group.add(message.reply_channel)
    async_to_sync(layer.group_add)(
        room.websocket_group,
        channel.channel_name,
    )
    channel.rooms = list(set(channel.rooms).union([room.id]))

    async_to_sync(layer.group_send)(room.websocket_group, {'type':'websocket.send', 'text':json.dumps({'joined':room.id,'user':user.id})})

    unr = models.Unread.objects.filter(user_id=user.id, room_id=room.id).first()

    lr_d = ""

    if not unr is None:
        lr_d = unr.date_str

    channel.send(json.dumps({
        "join": str(room.id),
        "readed": lr_d,
        "title": room.title,
    }))
    #r = redis.StrictRedis(host=settings.loccon.REDIS_HOST, port=settings.loccon.REDIS_PORT)
    #js = getmessagesrd(str(room.id),r)
    js=[]
    
    if (unr is None):
        jspg = list(getmessagespg(timest,str(room.id),ARCHIVE_SIZE))
        if len(jspg)>0:
            read(jspg[0], user, channel)
    else:
        jspg = list(getmessagespg(unr.date,str(room.id),ARCHIVE_SIZE,'up'))
        if ((unr.date is None) and len(jspg)>0):
            read(jspg[0], user, channel)

    if (len(jspg)==0):
        channel.send(json.dumps({
            "room":str(room.id),
            "msg_type":c_set.MSG_TYPE_NO_MES,
        }))

    """
    for i in js:
        mes = json.loads(i) #каждое извлечённое из редиса сообщение отпрвляется по вебсокету
        uname = cl.get_u_name(mes['userid'])
        channel.send(json.dumps({
            "room":str(room.id),
            "username":uname,
            "userID":str(mes['userid']),
            "date":mes['date'],
            "message":mes['text'],
            "msg_type":c_set.MSG_TYPE_ARCHIVE,
        }))
    """

    for i in enumerate(jspg):
        ind = i[0]
        dbmes=i[1]
        m_dict = dbmes.to_dict
        prev_id=-1
        next_id=-1
        if (ind>0):
            prev_id = jspg[ind-1].id
        if (ind<len(jspg)-1):
            next_id = jspg[ind+1].id
        m_dict["msg_type"] = c_set.MSG_TYPE_ARCHIVE
        m_dict['prev_id'] = prev_id
        m_dict['next_id'] = next_id
        m_dict['own'] = int(m_dict['userID']==user.id)
        channel.send(json.dumps(m_dict))
        #read(dbmes, user, channel)
        """
        uname = cl.get_u_name(dbmes.sender_id)
        dbdate = dbmes.date_str
        Check = True
        for j in js:
            rdmes=json.loads(j)
            if (dbdate==rdmes['date'])and(dbmes.sender_id==rdmes['user_id']):
                #Check = False
                #checkings
                if (dbmes.message!=rdmes['text'] or dbmes.sender_id!=rdmes['user_id']): #если текст сообщения или имя пользователя не совпадают
                    #r.delete('chatmess:' + rdmes['room'] + '|' + str(rdmes['user_id']) + '>' + rdmes['date'])
                    name = 'chatmess:' + dbmes.room.id + '|' + str(dbmes.sender_id) + '>' + dbdate
                    #r.set(name, dbmes.message)
                    #r.expire(name, REDIS_MESSAGE_EXPIRE_TIME)
                    channel.send(json.dumps({
                            "room":str(dbmes.room.id),
                            "username":uname,
                            "userID":str(dbmes.sender_id),
                            "date":dbdate,
                            "message":dbmes.message,
                            "msg_type":c_set.MSG_TYPE_ARCHIVE,
                    }))
                break
        if (Check): #если в памяти редиса сообщение найдено не было, оно берётся из базы (отключено)
            m_dict = dbmes.to_dict
            m_dict["msg_type"] = c_set.MSG_TYPE_ARCHIVE
            channel.send(json.dumps(m_dict))
        """
    if not(unr is None):
        if(len(jspg)<ARCHIVE_SIZE):
            lm_id = -1
            if (len(jspg)>0):
                lm_id=jspg[0].id
            channel.send(json.dumps({
                "room":str(room.id),
                "message":lm_id,
                "msg_type":c_set.MSG_TYPE_NEWEST,
            }))
        if (len(jspg)>0):
            chat_getarchive(channel,{'dir':'down','room':room.id,'date':jspg[0].date_str})

def chat_leavejoin(channel,data):
    """
    подключение к другому чату
    вынесено в отдельную функцию так как при вызове со стороны
    клиента последовотельно команд покидния и входа в чат они могли
    выполняться в неверной последовательности
    """
    data["room"] = data["from"]
    chat_leave(channel,data)

    data["room"] = data["to"]
    chat_join(channel,data)

def chat_leave(channel,data):
    """
    """
    user = channel.user
    user_full_name = cl.get_u_name(user.id)
    room = models.Room.objects.filter(id=data.get('room',-1)).first()
    if (room is None):
        return

    rd_leave(channel,room.id)
    #if NOTIFY_USERS_ON_ENTER_OR_LEAVE_ROOMS:
    #    room.send_message(None, user, user_full_name, c_set.MSG_TYPE_LEAVE)

    async_to_sync(layer.group_discard)(
        room.websocket_group,
        channel.channel_name,
    )
    channel.rooms = list(set(channel.rooms).difference([room.id]))
    channel.send(json.dumps({
        "leave": str(room.id),
    }))

    if not(rd_check_join(user.id,room.id)):
        async_to_sync(layer.group_send)(room.websocket_group, {'type':'websocket.send', 'text':json.dumps({'left':room.id,'user':user.id})})

def chat_getarchive(channel,data):
    cmd = data.get('dir','up')
    user = channel.user
    m_id = data.get('id',-1)
    r_id = data.get('room',-1)
    if (m_id==-1):
        date = data.get('date',None)
    else:
        mes = models.ChatMessage.objects.filter(id=m_id).first()
        if mes is None:
            date = None
        else:
            date = mes.date
    jspg = list(getmessagespg(date,str(r_id),ARCHIVE_SIZE+1, cmd))
    cnt = 0
    for i in enumerate(jspg[:-1]):
        cnt += 1
        ind = i[0]
        dbmes=i[1]
        m_dict = dbmes.to_dict
        prev_id=-1
        next_id=-1
        if (ind>0):
            prev_id = jspg[ind-1].id
        if (ind<len(jspg)-1):
            next_id = jspg[ind+1].id
        m_dict["msg_type"] = c_set.MSG_TYPE_ARCHIVE
        #print(prev_id, m_dict['id'], next_id )
        m_dict['prev_id'] = prev_id
        m_dict['next_id'] = next_id
        m_dict['own'] = int(m_dict['userID']==user.id)
        if (ind==0)and(cmd=='newest'):
            m_dict['scroll_to']='1'
        channel.send(json.dumps(m_dict))
    if len(jspg)<ARCHIVE_SIZE:
        lm_id = -1
        if (len(jspg)>0):
            lm_id=jspg[0].id
        if (cmd=='down'):
            m_type = c_set.MSG_TYPE_NEWEST
        else:
            m_type = c_set.MSG_TYPE_OLDEST
        channel.send(json.dumps({
            "room":str(r_id),
            "message":lm_id,
            "msg_type":m_type,
        }))

def chat_list(channel,data):
    """
    список чатов
    """
    #celeryobj = celery.Celery(broker=BROKER_URL, backend=BROKER_URL)
    #res = celeryobj.send_task("chatapp.tasks.roomlist", kwargs={'userid' : str(message.user.id)})
    #rooms = serializers.deserialize("json",res.get())
    r = classes.get_redis_con()
    user = channel.user
    rooms = models.ChatMembers.objects.filter(user_id = user.id, chat__is_active=1)
    chatlist = []
    for room in rooms:
        async_to_sync(layer.group_add)(
            room.chat.websocket_group_stb,
            channel.channel_name,
        )
        unread = 0
        last = {}
        lastread = models.Unread.objects.filter(user_id = user.id, room = room.chat).first()
        lastother = models.Unread.last_read(room.chat,user)
        if not(lastread is None):
            unread = models.ChatMessage.objects.order_by("date").filter(date__gt = lastread.date, room_id=room.chat, is_active = 1).count()
            last = models.ChatMessage.objects.order_by("date").filter(room_id=room.chat, is_active = 1).last()
            if not(last is None):
                last = last.preview
            else:
                last = {}
        members = models.ChatMembers.objects.filter(chat_id = room.chat.id, chat__is_active=1).values_list('user_id', 'role')
        members_list = []
        for memeber in members:
            m_id = memeber[0]
            m_obj = User.objects.filter(id=m_id).first()
            if not(m_obj is None):
                members_list.append({
                    'id':m_id,
                    'name':cl.get_u_name(m_obj),
                    'role':memeber[1],
                    'online':rd_check_online(m_id, r),
                    'join':rd_check_join(m_id, room.chat.id, r)
                })
        chatlist.append({
            'id': room.chat.id,
            'name' : room.chat.title,
            'role' : room.role,
            'perm' : room.get_room_perm(room.role),
            'unread' : unread,
            'last' : last,
            'lastother':lastother,
            'members':members_list,})
    channel.send(json.dumps({
        "list": 1,
        "rooms":chatlist,
    }))