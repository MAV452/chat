from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.db.models import Q
from django.contrib.auth import models as auth_models
import json, datetime, redis

from django.conf import settings

from django.utils.translation import gettext as _

from django.utils import timezone

from . import models

from . import ext_models

from .decorators import s_id_from_params

from django.contrib.auth.models import User, AnonymousUser

from django.conf import settings

from django.views.i18n import JavaScriptCatalog
from django.utils.translation.trans_real import DjangoTranslation, get_language

from django.contrib.auth.hashers import make_password, PBKDF2PasswordHasher

from django.contrib.sessions.models import Session

import base64
import hashlib
import hmac



def attach_perm(perm, user=None):
    if ('chatapp.add_attachment' in perm):
        return True
    return False

def themes_perm(perm, user=None):
    return True

def chat_create_perm(perm, user=None):
    if ('chatapp.add_room' in perm):
        return True
    return False

FUNC_ITEMS = {
    "file_attach":{'name':_("Attaching files"),'method':attach_perm},
    "themes":{'name':_("Color themes"),'method':themes_perm},
    "chat_create":{'name':_("Create rooms"),'method':chat_create_perm},
    #"chat_delete":{'name':_("Delete rooms"),'method':chat_delete_perm},
}

def is_allowed(auth_id,item):
    """
        Проверяет имеется ли у пользователя разрешение item используя соответствующую
        ему функцию method прописанный в FUNC_ITEMS

        Если item заканчивается символом '/' то считается что это пункт меню
        и проверяется что имеется любое из разрешений указанных в
        соответствующем ему items прописанных в MENU_ITEMS
    """
    if not(settings.USE_PERMISSIONS):
        return True
    user = auth_models.User.objects.filter(id = auth_id).first()
    if (user is None):
        perm = []
    else:
        perm = user.get_all_permissions()
    #if (len(item)>0)&(item[-1]=='/'):
    #    ob = MENU_ITEMS.get(item,None)
    #    if not(ob is None):
    #        for p_item in ob['items']:
    #            if FUNC_ITEMS[p_item]['method'](perm,user):
    #                return True
    #else:
    #    return FUNC_ITEMS[item]['method'](perm,user)
    return FUNC_ITEMS[item]['method'](perm,user)

    return False

def get_permissions(user):
    """
        Возвращает список из всех разрешений ползователя из списка MENU_ITEMS

        Имеется ли разрешение определяется по возврату функции указанной в method
    """
    out=[]
    if (user is None):
        permissions = []
    else:
        permissions = user.get_all_permissions()
    #for item in permissions:
    #    print(item)
    for func_item in FUNC_ITEMS.keys():
        if (FUNC_ITEMS[func_item]['method'](permissions, user)) or (not settings.USE_PERMISSIONS):
            out.append(func_item)
    return out

def get_redis_con():
    """
    """
    if (len(settings.REDIS_SOCKET_DIR)==0):
        return redis.StrictRedis(
            host=settings.REDIS_HOST,
            port=settings.REDIS_PORT,
            db=settings.REDIS_DB,
            )
    else:
        return redis.StrictRedis(
            unix_socket_path=settings.REDIS_SOCKET_DIR,
            db=settings.REDIS_DB
        )

#Расшифровка сессии с явным указанием секретного ключа приложения
def session_utoken(msg, secret_key, class_name='SessionStore'):
    key_salt = "django.contrib.sessions" + class_name
    sha1 = hashlib.sha1((key_salt + secret_key).encode('utf-8')).digest()
    utoken = hmac.new(sha1, msg=msg, digestmod=hashlib.sha1).hexdigest()
    return utoken


def decode(session_data, secret_key, class_name='SessionStore'):
    encoded_data = base64.b64decode(session_data)
    utoken, pickled = encoded_data.split(b':', 1)
    expected_utoken = session_utoken(pickled, secret_key, class_name)
    if utoken.decode() != expected_utoken:
        raise BaseException('Session data corrupted "%s" != "%s"',
                            utoken.decode(),
                            expected_utoken)
    return json.loads(pickled.decode('utf-8'))

def check_chatauth(session_key):
    """
    Поиск авторизованных сессий среди объектов chatauth
    Получчает на вход параметр session_key - session_key сессии джанго и пытается извлечь
    из него ИД пользователя СУРа, при неудаче ищет данные авторизации среди объектов chatauth
    Возвращает объект User или None
    Если указанный в chatauth пользователь чата не найден ищет указанного пользователя СУРа
    Если нет соответствующего пользователю СУРа пользователя чата, создаёт его
    """
    user = None
    if (settings.SUR_AUTH):
        session = Session.objects.filter(session_key=session_key).first()
        if not(session is None):
            try:
                session_data = decode(session.session_data,settings.SUR_SECRET_KEY)
            except:
                try:
                    session_data = decode(session.session_data,settings.SECRET_KEY)
                except:
                    uid_s=-1
                finally:
                    uid_s=session_data.get('_auth_user_id',-1)
            finally:
                uid_s=session_data.get('_auth_user_id',-1)
            if (uid_s!=-1):#если в данных сессии есть ИД пользователя, считаем что это ИД пользователя СУРа
                ext_user = ext_models.ext_user.objects.filter(id=uid_s).first()
                if not(ext_user is None):
                    user = None
                    uis = models.UserIsSur.objects.filter(s_u_id=ext_user.id).first()
                    if not(uis is None):#ищем метку что перенесён из СУРа
                        user = uis.user
                    if (user is None):
                        if not(User.objects.filter(username=ext_user.username).exists()):#если логин свободен
                            user = User.objects.create_user(
                                username=ext_user.username,
                                password=ext_user.password,
                                email=ext_user.email,
                                is_superuser=ext_user.is_superuser,
                                is_staff=ext_user.is_staff,
                                is_active=ext_user.is_active,
                                )
                            user.save()
                            uis = models.UserIsSur(user=user, s_u_id=ext_user.id)
                            uis.save()
                    else:
                        uis = models.UserIsSur.objects.filter(user_id=user.id).first()
                    if (uis is None):
                        user=None
                    elif (uis.s_u_id!=ext_user.id):
                        user=None
        
        if not(user is None):
            return user

    if not(session_key is None):
        ca_o = models.ChatAuth.objects.filter(key=session_key).last()
        if not(ca_o is None):
            user = User.objects.filter(id=ca_o.uid).first()
            if user is None:
                if (settings.SUR_AUTH):
                    ext_user = ext_models.ext_user.objects.filter(id=ca_o.uid_s).first()
                    if not(ext_user is None):
                        user = None
                        uis = models.UserIsSur.objects.filter(s_u_id=ext_user.id).first()
                        if not(uis is None):
                            user = uis.user
                        if (user is None):
                            if not(User.objects.filter(username=ext_user.username).exists()):
                                user = User.objects.create_user(
                                    username=ext_user.username,
                                    password=ext_user.password,
                                    email=ext_user.email,
                                    is_superuser=ext_user.is_superuser,
                                    is_staff=ext_user.is_staff,
                                    is_active=ext_user.is_active,
                                    )
                                user.save()
                                uis = models.UserIsSur(user=user, s_u_id=ext_user.id)
                                uis.save()
                        else:
                            uis = models.UserIsSur.objects.filter(user_id=user.id).first()
                        if (uis is None):
                            user=None
                        elif (uis.s_u_id!=ext_user.id):
                            user=None
                        else:
                            ca_o.uid = user.id
                            ca_o.save()

    return user

def get_user(request):
    """
    Возвращает объект user запроса
    """
    user = None
    session = request.COOKIES.get(settings.SESSION_COOKIE_NAME,None)
    user = check_chatauth(session)
    if user is None:
        return request.user

    return user

def get_u_name(user):
    """
        Возвращает имя пользователя
        Если входной параметр является числом ищет пользователя с таким ИД
        Имя ппользователя ищется в следующем порядке:

        ФИО объекта client_user(в базе СУРа) > ФИО объекта sentry_user (в баззе СУРа)
        > почтовый ящик пользователя > логин пользователя
        Если пользователь не автироризован возвращает "---"
    """
    if (isinstance(user,int)):
        user = User.objects.filter(id=user).first()
    if (user is None)or not(user.is_authenticated):
        return '---'
    e_user = None
    if (settings.SUR_AUTH):
        uis = models.UserIsSur.objects.filter(user_id=user.id).first()
        if not(uis is None):
            e_user = ext_models.ext_user.objects.filter(id=uis.s_u_id).first()
    if (e_user is None):
        if (user.email):
            return user.email
        else:
            return user.username
    cl_u = ext_models.client_user.objects.filter(auth_id = e_user.id).first()
    if not(cl_u is None):
        return cl_u.full_name
    s_u = ext_models.sentry_user.objects.filter(auth_id = e_user.id).first()
    if not(s_u is None):
        return s_u.full_name
    if (e_user.email):
        return e_user.email
    return e_user.username

def password_hash(pas, salt, iter, alg):
    """
    """
    out=''
    if (alg=='pbkdf2_sha256'):
        hsr = PBKDF2PasswordHasher
        out = hsr.encode(hsr,pas,salt,int(iter))
    else:
        raise Exception('password alg error')

    return out

@s_id_from_params
def login_user(request, user):
    """
    """
    user_id = -1
    if (isinstance(user,int)):
        user_obj = User.objects.filter(id=user).first()
        if not(user_obj is None):
           user_id = user_obj.id
        
    elif (isinstance(user, User)):
        user_id = user.id
    s_id = request.COOKIES.get(settings.SESSION_COOKIE_NAME)
    if s_id is None:
        request.session.create()
        s_id = request.session.session_key
    ca_o = models.ChatAuth(key=s_id, uid=user_id, date=datetime.datetime.now())
    ca_o.save()

def get_index(request,template):
    """
    Возвращает страницу по шаблону template с добавлением необходимых параметров в контекст
    Контекст содержит:
    Если успешно:
    title - заголовок страницы
    version - версия ЛК
    u_name - имя текущего пользователя
    theme - текущая цветовая тема
    """

    link_base = request.path[1:]
    title = _("WMS Account")

    user=get_user(request)
        
    return render(request, template, {
        'title':title,
        'version':settings.VERSION,
        'use_sur':int(settings.SUR_AUTH),
        'u_name':get_u_name(user),
        'theme':json.dumps(get_themes(user.id,True)),
    })

def datestr_to_datetime(str):
    """
    Переводит дату в строковом виде в объект datetime
    Дата принмается в формате ГГГГ-ММ-ДД 
    """
    return timezone.make_aware(datetime.datetime.strptime(str, "%Y-%m-%d"))

def get_lang(user_id=-1):
    """
        Возвращает название языка указанное в настроках пользователя из модели pers_settings
        Если не выходит берёт значение из настроек проекта, если также не выходит возвращает 'en'
        получает на вход параметр user_id - ИД пользователя (по умолчанию -1)
    """
    set_o = models.pers_settings.get_settings(user_id)
    if (not(set_o is None)and(set_o.Lang!='')):
        return set_o.Lang
    if (settings.LANGUAGE_CODE):
        return settings.LANGUAGE_CODE
    return 'en'

def get_themes(u_id,cur):
    """
        Возвращает список доступных текущему пользователю цветовых тем

        Принимает на вход два параметра:
            u_id:ИД объекта user
            cur:возвращать все темы или только текущую

        Возвращаемый список меет следющий формат:
        {
            'list':[
                {
                    'id':ИД темы,
                    'name':Название темы,
                    'light':{цвета светлого варианта темы
                        'кодовое название цвета':{
                            'name':название цвета,
                            'value':код цвета вида #ffffff
                        },
                        ...
                    },
                    'dark':{цвета тёмного варианта темы
                        'кодовое название цвета':{
                            'name':название цвета,
                            'value':код цвета вида #ffffff
                        },
                        ...
                    },
                    'cur':использует ли текущий пользователь эту темы сейчас
                },
                ...
            ],
            'dark':используется ли в данный момент светлая или тёмная тема
        
        }
    """
    out = []
    dark = False
    cur_id = -1
    set_o = models.pers_settings.get_settings(u_id)
    if not (set_o is None):
        dark = set_o.Theme_dark
        if not(set_o.Theme is None):
            cur_id = set_o.Theme.id
        else:
            st_t = models.color_themes.objects.filter(user_id=-1).first()
            if not(st_t is None):
                cur_id = st_t.id
    if (set_o is None):
        st_t = models.color_themes.objects.filter(user_id=-1).first()
        if not(st_t is None):
            cur_id = st_t.id
    if (not cur):
        themes = models.color_themes.objects.filter(user_id__in=[-1,u_id])
    else:
        themes = models.color_themes.objects.filter(id=cur_id)
    color_list = models.color_themes_colors.objects.all()
    st_colors = {}
    st_theme = models.color_themes.objects.filter(user_id=-1).first()
    for color in color_list:
        color_o = models.color_themes_color_theme.objects.filter(theme=st_theme,color=color).first()
        st_colors[color.code_name]=color_o
    for theme in themes:
        dct = {
            'id':theme.id,
            'name':theme.name,
            'light':{},
            'dark':{},
            'cur':(theme.id==cur_id),
        }
        colors = models.color_themes_color_theme.objects.filter(theme=theme)
        for color_o in color_list:
            color = colors.filter(color__code_name=color_o.code_name)
            color = color.first()
            if (not color is None):
                if (len(color.light)>0):
                    dct['light'][color_o.code_name]={
                        'name':color_o.name,
                        'value':color.light,
                    }
                else:
                    dct['light'][color_o.code_name]={
                        'name':color_o.name,
                        'value':st_colors[color_o.code_name].light,
                    }
                if (len(color.dark)>0):
                    dct['dark'][color_o.code_name]={
                        'name':color_o.name,
                        'value':color.dark,
                    }
                else:
                    dct['dark'][color_o.code_name]={
                        'name':color_o.name,
                        'value':st_colors[color_o.code_name].dark,
                    }
        out.append(dct)

    return ({'list':out, 'dark':dark})


class JavaScriptCatalogSwitch(JavaScriptCatalog):
    """
        Переобпределение функции get в JavaScriptCatalog с вынесением языка
        в параметр lang (по умолчанию значение получается из get_language())
    """
    lang = get_language()

    def get(self, request, *args, **kwargs):
        locale = self.lang
        domain = kwargs.get('domain', self.domain)
        # If packages are not provided, default to all installed packages, as
        # DjangoTranslation without localedirs harvests them all.
        packages = kwargs.get('packages', '')
        packages = packages.split('+') if packages else self.packages
        paths = self.get_paths(packages) if packages else None
        self.translation = DjangoTranslation(locale, domain=domain, localedirs=paths)
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)