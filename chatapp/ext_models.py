# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User, UserManager, AbstractUser
from django.contrib.postgres.fields import JSONField

class ext_user(AbstractUser):

    groups=None
    user_permissions = None

    objects = UserManager()

    class Meta:
        managed = False
        db_table = 'auth_user'

class dir_user_post(models.Model):
    name = models.CharField(max_length=128, help_text='Должность')
    is_active = models.SmallIntegerField(default=1)

    def __unicode__(self):
        return self.name

    class Meta:
        db_table = 'dir_user_post'
        ordering = ['name']
        default_permissions = ()
        managed = False

class dir_user_status(models.Model):
    name = models.CharField(max_length=128, verbose_name='Статус')
    position = models.PositiveSmallIntegerField(null=True, blank=True, verbose_name='Сортировка')

    class Meta:
        db_table = 'dir_user_status'
        ordering = ['position', 'name']
        default_permissions = ()
        managed = False

    def __unicode__(self):
        return self.name

class sentry_user(models.Model):
    auth = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    full_name = models.CharField(max_length=128)
    birthday = models.DateTimeField(null=True, blank=True)
    growth = models.PositiveSmallIntegerField(null=True, blank=True, help_text='Рост')
    weight = models.PositiveSmallIntegerField(null=True, blank=True, help_text='Вес')
    rank = models.CharField(max_length=32, blank=True, help_text='Ранг, разряд, степень')
    username = models.CharField(max_length=128, blank=True)
    user_post = models.ForeignKey(dir_user_post, null=True, blank=True, help_text='Должность', on_delete=models.SET_NULL)
    status = models.ForeignKey(dir_user_status, null=True, on_delete=models.SET_NULL)
    mobile_phone = models.CharField(max_length=24, blank=True)
    city_phone = models.CharField(max_length=24, blank=True)
    other_phone = models.CharField(max_length=24, blank=True)
    email = models.CharField(max_length=256, blank=True)
    passport_series = models.CharField(max_length=4, blank=True, help_text='Серия паспорта')
    passport_number = models.CharField(max_length=9, blank=True, help_text='Номер паспорта')
    passport_data = models.TextField(null=True, blank=True, help_text='Кем и когда выдан')
    address_placement = models.CharField(max_length=128, null=True, blank=True, help_text='Помещение')
    address = models.CharField(max_length=256, blank=True)
    address2 = models.CharField(max_length=256, blank=True)
    photo = models.FileField(upload_to='upload/user', null=True, blank=True, verbose_name='Файл')
    comment = models.TextField(null=True, blank=True)
    ip = models.CharField(max_length=15, default='', blank=True)
    experience = models.CharField(max_length=128, default='', blank=True, help_text='Стаж')
    json_data = JSONField(default={})
    is_active = models.SmallIntegerField(default=1)

    list_search_fields = dict(
        full_name='__icontains',
        auth='__username__icontains'
    )

    class Meta:
        db_table = 'sentry_user'
        ordering = ['full_name']
        default_permissions = ()
        managed = False

class client_user(models.Model):
    auth = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    full_name = models.CharField(max_length=256)
    priority = models.IntegerField(default=1)
    post = models.ForeignKey(dir_user_post, blank=True, null=True, on_delete=models.SET_NULL)
    birthday = models.DateTimeField(blank=True, null=True)
    passport = models.CharField(max_length=256, blank=True, null=True)
    #address_building = models.ForeignKey(dir_address_4_building, null=True, blank=True, help_text=u'Здание', on_delete=models.SET_NULL)
    address_placement = models.CharField(max_length=128, null=True, blank=True, help_text=u'Помещение')
    address = models.CharField(max_length=512, blank=True, null=True, help_text=u'Адрес')
    comment = models.TextField(blank=True, help_text=u'Комментарий')
    is_active = models.SmallIntegerField(default=1)
    #client_user_phone = models.ManyToManyField(client_user_phone)
    #client_user_email = models.ManyToManyField(client_user_email)
    data = JSONField(default={'object_key': {}, 'bind_subscribe': []})

    #addresses = models.ManyToManyField('org.Address',
    #                                   through='org.AddressLink', through_fields=('client_user', 'address'))

    class Meta:
        db_table = 'client_user'
        ordering = ['full_name']
        default_permissions = ()
        permissions = (
            ('client_user', 'Доступ к ответственным лицам'),
            ('add_client_user', 'Добавлять ответственные лица'),
            ('change_client_user', 'Редактировать ответственные лица'),
            ('delete_client_user', 'Удалять ответственные лица'),
        )
        managed = False

    def __str__(self):
        return self.full_name

    def __unicode__(self):
        return self.full_name
