/* */
export default{
    data(){
        return{
            loading:false,
            //флаг отобрвжения колеса загрузки
            theme_set:false,
            s_bar:false,
            message:'',
            timeout:5000,
            s_color:'info',
            //параметры для всплывающей панели сообщений
            date_format_options:{
                year: 'numeric',
                month: '2-digit',
                day: '2-digit',
            },
            time_format_options:{
                hour: '2-digit',
                minute: '2-digit',
                second: '2-digit',
            },
            rst:true,
            loaded:false,
            u_notif:[],
            //флаг определяющий загружены ли данные модуля (используется у раскрываемых модулей при их открытии)
        }
    },
    computed:{
        host(){
            if (typeof(chaturl) != 'undefined'){
                return chaturl
            }
            return window.location.host+'';
        },
        host_or_empty(){
            if (typeof(chaturl) != 'undefined'){
                return 'https://'+chaturl
            }
            return ''
        },
        is_mobile(){
            //return true;
            return (typeof window.orientation !== 'undefined');
        },
        m_class(){
            if (this.is_mobile){
                return 'mobile'
            }
            return ''
        },
        menu_btn_size(){
            return ''
            if (this.is_mobile){
                return Number(getComputedStyle(document.documentElement).getPropertyValue('--menu-btn-size').replace('px',''));
            }
            return ''
        },
        btn_size(){
            return ''
            if (this.is_mobile){
                return Number(getComputedStyle(document.documentElement).getPropertyValue('--btn-size').replace('px',''));
            }
            return ''
        },
        btn_size_tree(){
            return ''
            if (this.is_mobile){
                return Number(getComputedStyle(document.documentElement).getPropertyValue('--btn-size').replace('px',''))*1.5;
            }
            return ''
        },
        icon_size(){
            return ''
            if (this.is_mobile){
                return Number(getComputedStyle(document.documentElement).getPropertyValue('--icon-size').replace('px',''));
            };
            return ''
        },
        iconbtnw_size(){
            return ''
            if (this.is_mobile){
                return this.icon_size*1.1;
            };
            return ''
        },
        iconbtn_size(){
            return ''
            if (this.is_mobile){
                return Number(getComputedStyle(document.documentElement).getPropertyValue('--iconbtn-size').replace('px',''));
            };
            return ''
        },
        label_font(){
            return ''
            if (this.is_mobile){
                return Number(getComputedStyle(document.documentElement).getPropertyValue('--label-font').replace('px',''));
            };
            return ''
        },
        header_font(){
            return ''
            if (this.is_mobile){
                return Number(getComputedStyle(document.documentElement).getPropertyValue('--header-font').replace('px',''));
            };
            return ''
        },
        regular_font(){
            return ''
            if (this.is_mobile){
                return Number(getComputedStyle(document.documentElement).getPropertyValue('--regular-font').replace('px',''));
            };
            return ''
        },
        sub_font(){
            return ''
            if (this.is_mobile){
                return Number(getComputedStyle(document.documentElement).getPropertyValue('--sub-font').replace('px',''));
            };
            return ''
        },
        scrollbar_color(){
            return this.get_color('scrollbar') + ' ' + this.get_color('scrollbar_bg');
        },
        app_height(){
            return window.innerHeight*0.8;
        },
        app_width(){
            return window.innerWidth;
        },
        snackbar_offset(){
            return 0;
            if(this.is_mobile){
                return 100;
            }
            return 0;
        },
        cur_user_name(){
            return this.$store.getters.get_context_item('u_name');
        },
        version(){
            return this.$store.getters.get_context_item('ver');
        },
        is_dark_theme(){
            return this.$vuetify.theme.dark;
        },
        rst_g(){
            return this.$store.getters.get_rst_g;
        },
    },
    methods:{
        //функция gettext
        _(item){
            //console.log('a')
            /*if (window.gettext){
                return window.gettext(item);    
            }
            return item;*/
            if (this.$store){
                return this.$store.getters.get_text(item);
            }
            return item;
        },
        //перерендер отдельного компонента
        reset(){
            Vue.set(this,'rst',false);
            var ths = this;
            Vue.nextTick(function(){Vue.set(ths,'rst',true);});
        },
        //заставляет всё перерендерить выключая и включая компоненты через v-if в их корневых div
        //используется для обновления всего текста при смене языка
        reset_g(){
            this.$store.commit('reset_g');
        },
        update(){
            this.$forceUpdate;
        },
        int_to_array(item){
            var out = [];
            out.push(item);
            return out;
        },
        //загрузка нового языка при его смене
        lang_reset(code=undefined){
            var script = document.getElementById('js_catalog');
            var script_n = document.createElement('script');
            script_n.id = script.id;
            var src = script.src;
            var p = src.indexOf('p?lang=');//удаляет параметр языка если есть
            if (p!=-1){
                src = src.substr(0,p);
            }
            if (code==undefined){//параметр не указан, определится сервером
                script_n.src = src;
            }else{//параметр указан при вызове функции 
                script_n.src = src+'p?lang='+code;
            }
            script_n.type = script.type;
            var ths=this;
            script_n.onload=function(){ths.$store.commit('drop_gtxt');};
            script.remove();
            delete(window.django);
            var head = document.getElementsByTagName('head')[0];
            head.appendChild(script_n);
        },
        //проверка прав
        permited(item){
            return this.$store.getters.is_permited(item);
        },
        date_to_str(date){
            if (date==undefined){
                return ''
            }
            if (!typeof date=='string' || date.length==0){
                return ''
            }
            var dt = new Date(date);
            return dt.toLocaleString("ru",this.date_format_options);
        },
        time_to_str(date){
            if (date==undefined){
                return ''
            }
            if (!typeof date=='string' || date.length==0){
                return ''
            }
            var dt = new Date(date);
            return dt.toLocaleString("ru",this.time_format_options);
        },
        datetime_to_str(date){
            if (date==undefined){
                return ''
            }
            if (!typeof date=='string' || date.length==0){
                return ''
            }
            var dt = new Date(date);
            return dt.toLocaleString("ru",this.time_format_options) + ' ' + dt.toLocaleString("ru",this.date_format_options);
        },
        //обработчик ошибок
        err_process(error){
            Vue.set(this,'loading',false);
            var c_name = this.$options._componentTag;
            if (c_name){
                this.err_mes(c_name+': '+error.message);
            }else{
                this.err_mes(error.message);
            }
            console.log(error);
        },
        //показать сообщение об ошибке
        err_mes(message){
            if(message=='No auth'){
                location.reload();
            }
            Vue.set(this,'s_color','error');
            Vue.set(this,'message',message);
            Vue.set(this,'s_bar',true);
            Vue.set(this,'loading',false);
            console.log(message);
        },
        err_mes_g(message){
            if(message=='No auth'){
                location.reload();
            }
            this.$store.commit('err_mes',message)
            console.log(message);
        },
        //показать сообщение об успешном выполнении действия
        suc_mes(message){
            Vue.set(this,'s_color','success');
            Vue.set(this,'message',message);
            Vue.set(this,'s_bar',true);
            Vue.set(this,'loading',false);
        },
        //показать сообщение
        info_mes(message){
            Vue.set(this,'s_color','info');
            Vue.set(this,'message',message);
            Vue.set(this,'s_bar',true);
            Vue.set(this,'loading',false);
        },
        //получение цвета текущей цветовой темы по его идентификатору
        get_color(item){
            if (this.$vuetify.theme.dark){
                if (this.$vuetify.theme.themes.dark[item]){
                    return this.$vuetify.theme.themes.dark[item];
                }
            }else{
                if (this.$vuetify.theme.themes.light[item]){
                    return this.$vuetify.theme.themes.light[item];
                }
            }
            return '';
        },
        set_theme(theme, n_r=false){
            for (var color in theme.light){
                Vue.set(this.$vuetify.theme.themes.light,color,theme.light[color].value);
            }
            for (var color in theme.dark){
                Vue.set(this.$vuetify.theme.themes.dark,color,theme.dark[color].value);
            }
            if (n_r){
                Vue.set(this,'theme_set',true);
            }
            var ths = this;
            Vue.nextTick(function(){
                Vue.nextTick(function(){
                    ths.highlight_css_init()
                })
            })
        },
        //получить и установить текущую тему пользователя
        get_cur_theme(){
            if (!this.permited('themes')){
                return;
            }
            var ths=this;
            if (!this.theme_set){
                var context = this.$store.getters.get_context_item('theme');
                if (context.length>0){
                    var theme = JSON.parse(context);
                    Vue.set(ths.$vuetify.theme,'dark',theme.dark);
                    ths.set_theme(theme.list[0]);
                }
            }
            var qs = ''
                if (typeof sessionid !='undefined'){
                    qs="&sessionid="+sessionid
                }
            axios.get(this.host_or_empty+'/list/themes/?cur'+qs, {withCredentials: true,}).then(function(result){
                if (result.data.error){
                    console.log(result.data.error);
                }else{
                    if (!(result.data.list&&result.data.list[0])){
                        return;
                    }
                    Vue.set(ths.$vuetify.theme,'dark',result.data.dark);
                    ths.set_theme(result.data.list[0]);
                }
            }).catch(error=>{ths.err_process(error);});
        },
        u_notif_close(item){
            var ind = this.u_notif.indexOf(item);
            if (ind>-1){
                this.u_notif.splice(ind,1);
            }
        },
        add_u_notif(item){
            var ind = -1;
            for (var i in this.u_notif){
                if (this.u_notif[i].text==item.text){
                    ind=i;
                }
            }
            if (ind==-1){
                this.u_notif.push(item);
            }
        },
        on_logout(nc=false, all=false){
            if (nc || confirm(this._('Log out?'))){
                var ths = this;
                var qs = ''
                if (all){
                    qs='?all=true'
                }
                if (typeof sessionid !=='undefined'){
                    if (qs.length>0){
                        qs+='&'
                    }else{
                        qs+='?'
                    }
                    qs+="sessionid="+sessionid
                }
                axios.get(this.host_or_empty+'/signout/'+qs, {withCredentials: true,}).then(function(){ths.$emit('logout')}).catch(error=>{ths.err_process(error);});
            }
        },
        to_root(){
            document.location.href="/";
        },
        highlight_css_init(){
            var ssheet = undefined;
            var flag = false;
            for(var i in document.styleSheets){
                ssheet = document.styleSheets[i];
                var rules = []
                try{
                    rules = (ssheet.cssRules || ssheet.rules)//Нельзя получить правила стиля,полученного не с того же сервера
                } catch(e){
                    //пропускаем такие стили
                }
                for (var j in rules){
                    if (rules[j].selectorText == '.chatdiv_main .mesdiv.highlight'){
                        flag = true;
                        break;
                    }
                }
                if (flag){
                    break;
                }
            }
            if (!flag){
                return;
            }

            var styles = {
                'point':this.get_color('font_main'),
                'error':this.get_color('error'),
            }
            for (var i in styles){
                var rules = []
                try{
                    rules = (ssheet.cssRules || ssheet.rules)
                } catch(e){
                    
                }
                var i1,i2=-1;
                for (var j in rules){
                    if (rules[j].selectorText == ".chatdiv_main .mesdiv.highlight."+i){
                        i1 = j;
                    }
                    if (rules[j].selectorText == "@keyframes highlight_flickr_"+i){
                        i2 = j;
                    }
                    if (i1>-1 && i2>-1){
                        break;
                    }
                }
                if (i1>-1){
                    ssheet.deleteRule(i1)
                }
                if (i2>-1){
                    ssheet.deleteRule(i2)
                }
                var style = ".chatdiv_main .mesdiv.highlight."+i+" {"
                style+="animation: highlight_flickr_"+i+" 4s 3;"
                style+="background-color: "+styles[i]+';'
                style+="}"
                ssheet.insertRule(style, rules.length);
                var animation = "@keyframes highlight_flickr_"+i+" {"
                animation += "0% {background-color: "+styles[i]+"ff;}"
                animation += "50% {background-color: "+styles[i]+"00;}"
                animation += "100% {background-color: "+styles[i]+"ff;}"
                animation += "}"
                ssheet.insertRule(animation, rules.length);
            }
        }
    }
}