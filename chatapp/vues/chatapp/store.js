import Vue from 'vue'
import vuex from 'vuex'
import axios from 'axios'
Vue.use(vuex);
window.store = new vuex.Store({
    state: {
      interval:null,
      permissions: [],
      context:{},
      gtxt:{},
      rst_g:true,
      errors:[],
      played:[],
      player_url:{},
      ws_state:3,
      connected:false,
    },
    mutations: {
      //глобальный перерендер
      reset_g(state){
        state.rst_g=false;
        Vue.nextTick(function(){state.rst_g=true;});
      },
      err_mes(state,message){
        state.errors.push(message);
      },
      err_after_pull(state,message){
        state.errors.splice(0,1);
      },
      //сброс списка значений для gettext
      drop_gtxt(state){
        Object.getOwnPropertyNames(state.gtxt).forEach(function (prop) {
          delete state.gtxt[prop];
        });
        Vue.nextTick(this._mutations.reset_g[0](state));
      },
      //изъятие данных передаваемых с формированием шаблона через скрытый div
      set_context(state,context){
        for (var item in context){
          state.context[context[item].name] = context[item].value;
        }
      },
      //задать список прав
      permition_update (state,vals){
        state.permissions=vals;
        console.log('b')
      },
      played_add(state,obj){
          if (state.played.indexOf(obj)==-1){
            state.played.push(obj);
          }
      },
      played_remove (state,obj){
        var ind = state.played.indexOf(obj);
        if (state.played.indexOf(obj)>-1){
          state.played.slice(ind,1);
        }
      },
      played_stop(state,obj){

      },
      played_close(state,obj){

      },
      player_set(state,url){
        state.player_url = url;
        console.log(state.player_url)
      },
      player_clear(state){
        state.player_url = {};
      },
      set_ws_state(state,val){
        state.ws_state = val;
      },
      set_connected(state,val){
        state.connected = val;
      },
    },
    actions:{

    },
    getters: {
      get_text: state => item => {
        if (state.gtxt[item]){
          return state.gtxt[item]
        }else{
          if (window.django && window.django.catalog[item]){
            state.gtxt[item]=window.django.catalog[item]
            return state.gtxt[item];
          }else{
            return item;
          }
        }
        return item;
      },
      get_ws_state: state => {return state.ws_state},
      get_connected: state => {return state.connected},
      get_played: state => {return state.played},
      get_player_url: state => {return state.player_url},
      get_errors: state => {return state.errors},
      pull_error: state => {if (state.errors.length>0){return state.errors[0]};return undefined;},
      get_rst_g: state => {return state.rst_g;},
      //данные передаваемые с сервера при создании страницы
      get_context_item: state => item => {return state.context[item]},
      is_permited: state => item => {return state.permissions.indexOf(item)!=-1},
    }
  });

function host_or_empty(){
  if (typeof(chaturl) != 'undefined'){
      return 'https://'+chaturl
  }
  return ''
}
const get_perm = async () => {
  try {
      var qs = ''
        if (typeof sessionid !=='undefined'){
            qs="?sessionid="+sessionid
        }
      const resp = axios.get(host_or_empty()+'/list/permissions/'+qs, {withCredentials: true,}).then(function(result){
        if (result.data.error){
          console.log(error)
          alert(error);
        }else{
          console.log('a')
          window.store.commit('permition_update', result.data.list);
        }
      });
  } catch (err) {
      // Handle Error Here
      console.error(err);
  }
};


get_perm();

console.log('c')
window.addEventListener("load",function(event) {
  if (typeof(chat_context_obj)!='undefined'){
    var attr = chat_context_obj;
    for (var i in attr){
      attr[i]={'name':i,'value':attr[i]}
    }
  }else{
    var c_div = document.getElementById("chat_context");
    var attr=c_div.attributes;
  }
  window.store.commit('set_context', attr);
},false);