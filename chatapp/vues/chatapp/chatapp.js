import Vue from 'vue'
//import vuex from 'vuex'
import axios from 'axios'
import App from 'chatapp/lib/chatapp.vue'
import Vuetify from 'chatapp/vuetify'

var chat_vm;
function chat_init(){
  chat_vm = new Vue({
    vuetify: Vuetify,
    //router: V_Router,
    store: window.store,
    el: '#chat-div',
    render: h=>h(App)
  })
};
if (typeof(chaturl) != 'undefined'){//если запущено через скрипт то ждёт пока создастся исходный элемент
  var checkExist = setInterval(function() {
      if (document.getElementById('chat-div')!=undefined) {
        console.log("Exists!");
        chat_init();
        clearInterval(checkExist);
      }
  }, 100); // check every 100ms
}else{
  $.when(
  $.ready
  ).then( ()=>{
    chat_init()
  });
}