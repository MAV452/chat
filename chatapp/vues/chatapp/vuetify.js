import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import ru from 'vuetify/lib/locale/ru'
import en from 'vuetify/lib/locale/en'
import '@mdi/font/css/materialdesignicons.css'

Vue.use(Vuetify);
var loc_str = navigator.language;
var dash_pos = loc_str.indexOf('-');
if(dash_pos == -1){
    dash_pos = 2;
};
var lanq1 = loc_str.substring(0,dash_pos);

const opts = {
    iconfont:'mdi',
    lang:{
        locales:{en,ru},
        current:lanq1,
    },
    theme:{
        options:{
            customProperties: true,
        },
    },
};

export default new Vuetify(opts)