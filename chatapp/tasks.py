# -*- coding: utf-8 -*-
#chatapp/tasks.py
from .models import ChatMessage, Room
from django.contrib.auth.models import User
from celapp import app
from django.core import serializers


@app.task
def send(message, user, room, time): #добавление сообщения в базу
    sender = User.objects.get(pk=user)
    roomobj = Room.objects.get(pk=room)
    ChatMessage.objects.create(sender = sender, room = roomobj, date = time, message = message)

@app.task
def roomlist(userid): #получение списка комнат
    rooms = serializers.serialize('json',Room.objects.order_by("title"))
    return rooms

@app.task
def getmessage(chatid, time, amount): #получение сообщений из базы
    messages = serializers.serialize('json',ChatMessage.objects.order_by("date").filter(room_id = chatid, date__lt = time)[:amount])
    return messages