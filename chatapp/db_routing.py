# -*- coding: utf-8 -*-

SUR_APPS = [] # модели от этих приложений будут браться из базы СУРа
SUR_MODELS = ['session', 'sentry_user', 'dir_user_status', 'dir_user_post'] # эти модели будут браться из базы СУРа

from django.conf import settings

class SurRouter(object):

    def is_sur(self, model):
        """
        """
        if (not settings.SUR_AUTH):
            return False
        if not (model._meta.managed):
            return True
        if (model._meta.app_label in SUR_APPS):
            return True
        if (model._meta.model_name in SUR_MODELS):
            return True
        return False

    def db_for_read(self, model, **hints):
        """
        """
        if self.is_sur(model):
            return 'sur'
        return 'default'
        return None

    def db_for_write(self, model, **hints):
        """
        """
        if self.is_sur(model):
            return 'sur'
        return 'default'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        """
        return True
        return None
    
    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        """
        if db!='default':
            return False
        return True
        return None