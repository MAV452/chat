
from django.db import migrations, models

from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.management import create_permissions

from chatapp.models import Permissions

from django import apps as ap

from django.core import exceptions

from django.utils.translation import gettext_noop as _
from django.utils.functional import lazy

PERMISSIONS = {
    1:'add_room',
    2:'add_attachment',
}

GROUPS = [
    {
        'title':'user',
        'permissions':[
            1,
            2,
        ],
    },
    {
        'title':'admin',
        'permissions':[
            1,
            2,
        ],
    }
]

def add_items(apps=None, schema_editor=None):
    """
    """
    for app_config in apps.get_app_configs():
        app_config.models_module = True
        create_permissions(app_config, verbosity=0)
        app_config.models_module = None
    #content_type = ContentType.objects.get_for_model(Permissions)
    for item in PERMISSIONS.keys():
        perm = Permission.objects.filter(codename=PERMISSIONS[item]).first()
        if (perm is None):
            raise Exception('permission does not exist')

    for item in GROUPS:
        group = Group.objects.filter(name=item['title']).first()
        if (group is None):
            group = Group.objects.create(name=item['title'])
        group.permissions.clear()
        for perm_id in item['permissions']:
            perm = Permission.objects.filter(codename=PERMISSIONS[perm_id]).first()
            if (perm is None):
                raise Exception('permission does not exist')
            group.permissions.add(perm)

    


class Migration(migrations.Migration):

    dependencies = [
        ('chatapp', '0002_set_theme_items'),
    ]

    operations = [
        migrations.RunPython(add_items),
    ]
