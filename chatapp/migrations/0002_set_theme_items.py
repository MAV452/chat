
from django.db import migrations, models

from django import apps as ap

from django.utils.translation import gettext_noop as _
from django.utils.functional import lazy


THEMES = [
    {
        'name':'Default',
        'light':{
            'primary':'#888888',
            'secondary':'#a5a5a5',
            'accent':'#888888',
            'error':'#ff0000',
            'info':'#ffff00',
            'success':'#22aa22',
            'warning':'#aa2222',
            'button':'#f5f5f5',
            'button_sel':'#f0f0f0',
            'background':'#ffffff',
            'message_bg':'#f5f5f5',
            'message_text_bg':'#e5e5e5',
            'background_secondary':'#eeeeee',
            'background_third':'#d5d5d5',
            'list_even': '#f5f5f5',
            'list_odd': '#e5e5e5',
            'scrollbar': '#999999',
            'scrollbar_bg': '#bbbbbb',
            'font_header': '#222222',
            'font_main': '#000000',
            'font_secondary': '#444444',
            'font_btn': '#000000',
            'font_btn_sel': '#000000',
        },
        'dark':{
            'primary':'#ffffff',
            'secondary':'#aaaaaa',
            'accent':'#888888',
            'error':'#ff0000',
            'info':'#ffff00',
            'success':'#22aa22',
            'warning':'#aa2222',
            'background':'#111111',
            'message_bg':'#222222',
            'message_text_bg':'#333333',
            'button':'#1e1e1e',
            'button_sel':'#1f1f1f',
            'background_secondary':'#222222',
            'background_third':'#444444',
            'list_even': '#222222',
            'list_odd': '#333333',
            'scrollbar': '#444444',
            'scrollbar_bg': '#222222',
            'font_header': '#aaaaaa',
            'font_main': '#ffffff',
            'font_secondary': '#dddddd',
            'font_btn': '#ffffff',
            'font_btn_sel': '#ffffff',
        }
    },
    {
        'name':'Blue',
        'light':{
            'primary':'#1da4d4',
            'secondary':'#000055',
            'accent':'#0000ff',
            'error':'#ff0000',
            'info':'#ff0000',
            'success':'#22aa22',
            'warning':'#aa2222',
            'background':'',
            'message_bg':'#186688',
            'message_text_bg':'#186d91',
            'button':'#ffffff',
            'button_sel':'#1fa3d4',
            'background_secondary':'#165672',
            'background_third':'#165672',
            'list_even': '#186688',
            'list_odd': '#186d91',
            'scrollbar': '#1da4d4',
            'scrollbar_bg': '#06334a',
            'font_header': '',
            'font_main': '#ffffff',
            'font_secondary': '',
            'font_btn': '#163f55',
            'font_btn_sel': '#ffffff',
        },
        'dark':{

        }
    },
]

COLOR_ITEMS = {
    'primary':_('Primary'),
    'secondary':_('Secondary'),
    'accent':_('Accent'),
    'error':_('Error'),
    'info':_('Info'),
    'success':_('Success'),
    'warning':_('Warning'),
    'background':_('Background'),
    'message_bg':_('Message background'),
    'message_text_bg':_('Message text background'),
    'button':_('Button'),
    'button_sel':_('Button'),
    'background_secondary':_('Background 2'),
    'background_third':_('Background 3'),
    'list_even': _('List item even'),
    'list_odd': _('List item odd'),
    'scrollbar': _('Scrollbar'),
    'scrollbar_bg': _('Scrollbar background'),
    'font_header': _('Text headers'),
    'font_main': _('Text main'),
    'font_secondary': _('Text secondary'),
    'font_btn': _('Button text'),
    'font_btn_sel': _('Button text selected'),
}

def add_items(apps=None, schema_editor=None):
    """
    """
    theme_m = ap.apps.get_model('chatapp','color_themes')
    color_m = ap.apps.get_model('chatapp','color_themes_colors')
    th_c_m = ap.apps.get_model('chatapp','color_themes_color_theme')
    for color_k in COLOR_ITEMS.keys():
        color_o = color_m.objects.filter(code_name = color_k).first()
        if (color_o is None):
            color_o = color_m(code_name= color_k, name = COLOR_ITEMS[color_k])
            color_o.save()
        elif (color_o.code_name != color_o):
            color_o.code_name = COLOR_ITEMS[color_k]
            color_o.save()
    for theme in THEMES:
        theme_o = theme_m.objects.filter(name = theme['name']).first()
        if (theme_o is None):
            theme_o = theme_m(name=theme['name'],user_id=-1)
            theme_o.save()
        th_c_m.objects.filter(theme_id = theme_o.id).delete()
        for color_k in theme['light'].keys():
            color_o = color_m.objects.filter(code_name=color_k).first()
            if (color_o is None):
                color_o = color_m(code_name = color_k, name = '')
                color_o.save()
            th_c_o = th_c_m(theme = theme_o, color = color_o, light = theme['light'][color_k], dark = '')
            th_c_o.save()
        for color_k in theme['dark'].keys():
            color_o = color_m.objects.filter(code_name=color_k).first()
            if (color_o is None):
                color_o = color_m(code_name = color_k, name = '')
                color_o.save()
            th_c_o = th_c_m.objects.filter(color=color_o, theme=theme_o).first()
            if (th_c_o is None):
                th_c_o = th_c_m(theme = theme_o, color = color_o, dark = theme['dark'][color_k], light = '')
            else:
                th_c_o.dark = theme['dark'][color_k]
            th_c_o.save()

class Migration(migrations.Migration):

    dependencies = [
        ('chatapp', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(add_items),
    ]
