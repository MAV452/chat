from django.http import HttpResponse
import json, asyncio
from . import classes
from django.conf import settings
from functools import wraps

def ajax_login_required(view_func):
    def wrap(request, *args, **kwargs):
        user=classes.get_user(request)
        if user.is_authenticated:
            return view_func(request, *args, **kwargs)
        out = json.dumps({ 'error': 'No auth' })
        return HttpResponse(out)
    return wrap

def s_id_from_params(view_func):
    """
    Записывает ИД сесссии из параметров GET или POST в куки для авторизации
    """
    def add_cookie(request):
        s_id = request.GET.get('sessionid', None)
        if not(s_id is None):
            request.COOKIES[settings.SESSION_COOKIE_NAME]=s_id
        else:
            s_id = request.POST.get('sessionid', None)
            if not(s_id is None):
                request.COOKIES[settings.SESSION_COOKIE_NAME]=s_id

    def wrap(request, *args, **kwargs):
        add_cookie(request)
        return view_func(request, *args, **kwargs)
    return wrap