# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from posixpath import split

from django.conf import settings
from chatapp import classes
from enum import unique

import os, io, sys

from django.utils.translation import gettext_lazy as _

from channels.layers import get_channel_layer

from django.db import models
from django.utils import timezone
import json
from .settings import MSG_TYPE_MESSAGE, MSG_TYPE_NEW
import celery
import datetime
from asgiref.sync import async_to_sync
from . import ext_models
from django.contrib.auth.models import User

from PIL import Image
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage as storage

from django.db.models import Q

import ffmpeg

from chatapp import settings as c_set

SESSION_DURATION = 15

class Permissions(models.Model):
    """
        модель без таблицы в базе для добавления разрешений
    """

    class Meta:
        managed = False
                
        default_permissions = ()

        permissions = (
            
        )

class UserIsSur(models.Model):
    """
    Список пользователей перенесённых из СУРа и ИД соответствующего им пользователя СУРа
    """

    user = models.ForeignKey(User,on_delete=models.CASCADE, unique=True)
    s_u_id = models.IntegerField(default=-1, unique=True)

class Room(models.Model):
    """
    A room for people to chat in.
    """

    # Room title
    title = models.CharField(max_length=255)

    # If only "staff" users are allowed (is_staff on django's User)
    staff_only = models.BooleanField(default=False)

    is_active = models.BooleanField(default=True)

    def save(self, *args, **kwargs):
        if (type(self).objects.filter(~Q(id=self.id),title=self.title,is_active=True).exists()):
            raise ValueError('Room with that title already exists')
        super().save(*args, **kwargs)

    class Meta:
        verbose_name = 'Комната чата'
        verbose_name_plural = 'комнаты чата'

    @property
    def websocket_group(self):
        """
        Returns the Channels Group that sockets should subscribe to to get sent
        messages as they are generated.
        """
        return ("room-%s" % self.id)

    @property
    def websocket_group_stb(self):
        """
            Группа для получения информации о новых сообщений теми
            кто в данный момент в сети, но не находятся в этом чате
        """
        return ("roomstb-%s" % self.id)

    @classmethod
    def websocket_group_stb_by_id(self,id):
        """
            Для получения названия группы по ИД без вызова объекта
        """
        return ("roomstb-%s" % id)

    @classmethod
    def websocket_group_by_id(self,id):
        """
            Для получения названия группы по ИД без вызова объекта
        """
        return ("room-%s" % id)

    def send_message(self, channel, message, msg_type=MSG_TYPE_MESSAGE):
        """
        Called to send a message to the room on behalf of a user.
        """
        mes = message.to_dict
        mes['msg_type'] = msg_type
        mes['own'] = int(channel.user.id == mes['userID'])
        final_msg = json.dumps(mes)
        layer = get_channel_layer()
        # Send out the message to everyone in the room
        grp = self.websocket_group
        async_to_sync(layer.group_send)(self.websocket_group, {'type':'websocket.send','text':final_msg})
        async_to_sync(layer.group_send)(self.websocket_group_stb, {
            'type':'websocket.send',
            'text':json.dumps({
                'new_mes':mes['room'],
                'user':mes['userID'],
                'mes':message.preview,
                })
            })

    def new_message(self, channel):
        final_msg = json.dumps({'room': str(self.id), 'msg_type': MSG_TYPE_NEW})
        layer = get_channel_layer()
        async_to_sync(layer.group_send)(self.websocket_group, {'type':'websocket.send','text':final_msg})

    def __str__(self):
        return self.title


class ChatMembers(models.Model): #кто в каком чате состоит

    ROLES = [
        (0,_('User')),
        (1,_('Admin')),
    ]

    PERM = [
        'add_users',#добавление/удаление пользоватлей в комнате
        'set_roles',#задавать роли пользователей в комнате
        'del_room',#удалить комнату
        'del_message',
        'del_message_self',
    ]

    user_id = models.IntegerField(default=-1)
    chat = models.ForeignKey(Room, on_delete=models.CASCADE)
    role = models.IntegerField(choices=ROLES, default=0)

    @classmethod
    def is_permited(self,role_id,perm):
        """
        """
        for item in self.get_room_perm(role_id):
            if item==perm:
                return True
        return False
    
    @classmethod
    def get_room_perm(self,role_id):
        """
        """
        out=[]
        if (role_id==0):
            return [
                'del_message_self',
            ]
        elif (role_id==1):
            return [
                'add_users',
                'set_roles',
                'del_room',
                'del_message',
                'del_message_self',
            ]
        return out

    @property
    def user(self):
        return User.objects.filter(id=self.user_id).first()

    class Meta:
        unique_together = ('user_id', 'chat',)
        verbose_name = 'Принадлежность пользователей к чатам'
        verbose_name_plural = 'Принадлежность пользователей к чатам'

    def __str__(self):
        if not(self.user is None) or not(self.user.is_authenticated):
            return ''
        return (self.user.username)

class Attachment (models.Model): #приложение к сообщению
    ATTACHMENT_TYPES = [
        ('none','None'),
        ('doc','Document'),
        ('img','Image'),
        ('vid','Video'),
        ('aud','Audio'),
    ]

    IMG_EXT = [
        'png',
        'jpg',
        'jpeg',
        'bmp',
        'gif',
    ]
    VID_EXT = [
        'mp4',
        'webm',
    ]
    AUD_EXT = [
        'mp3',
    ]
    THUMB_SIZE = 300

    room = models.ForeignKey(Room, blank=True, null=True, on_delete=models.SET_NULL)
    file = models.FileField(null=True, blank=True, upload_to=settings.UPLOAD_DIR)
    preview = models.FileField(null=True, blank=True, upload_to=settings.UPLOAD_DIR)
    data = models.JSONField(null=True)
    type = models.CharField(choices=ATTACHMENT_TYPES, default='none', max_length=10)
    is_active = models.SmallIntegerField(default=1)

    @classmethod
    def gen_thumb_all(self):
        f = self.objects.all()
        for obj in f:
            if (obj.type in ['img', 'vid'] and obj.preview.name is None):
                obj.gen_thumb()

    def save(self, *args, **kwargs):
        self.calc_type()
        super(Attachment,self).save(*args, **kwargs)
        if (self.type in ['img', 'vid'] and self.preview.name is None):
            self.gen_thumb()

    def gen_thumb(self):
        """
            Генерация сжатого изображения для предварительного просмотра
        """
        if (self.type=='vid'):
            try:
                print(self.file.path)
                thumb_name, thumb_extension = os.path.splitext(self.file.name)
                thumb_extension = '.png'
                thumb_filename = thumb_name + '_thumb' + thumb_extension
                thumb_filename = settings.MEDIA_ROOT+'/'+ thumb_filename
                image = Image.open(io.BytesIO((
                        ffmpeg
                        .input(self.file.path)
                        .filter('scale', 300, -1)
                        .output('pipe:', vframes=1, format='image2', vcodec='png')
                        #.output(thumb_filename, vframes=1)
                        .run(capture_stdout=True)
                )[0])).convert('RGB')
            except:
                return
        else:
            try:
                image = Image.open(self.file.path).convert('RGB')
            except:
                return
        
        image.thumbnail((5000,self.THUMB_SIZE),Image.ANTIALIAS)

        thumb_name, thumb_extension = os.path.splitext(self.file.name)
        thumb_extension = thumb_extension.lower()

        if not(thumb_extension[1:] in self.IMG_EXT):
            thumb_extension = '.png'

        thumb_filename = thumb_name + '_thumb' + thumb_extension
        thumb_filename = thumb_filename.split('/')
        thumb_filename = thumb_filename[len(thumb_filename)-1]

        if (thumb_extension) in ['.jpg', '.jpeg']:
            FTYPE = 'JPEG'
        elif thumb_extension == '.gif':
            FTYPE = 'GIF'
        elif thumb_extension == '.png':
            FTYPE = 'PNG'
        elif thumb_extension == '.bmp':
            FTYPE = 'BMP'
        else:
            return

        temp_thumb = io.BytesIO()
        image.save(temp_thumb, FTYPE)
        temp_thumb.seek(0)

        self.preview.save(thumb_filename, ContentFile(temp_thumb.read()))
        temp_thumb.close()
        
    def calc_type(self,file=None,data=None):
        """
            Определенить тип приложения
        """
        if not(self.file is None):
            ext = split(self.file.name)[1].split('.')
            if (len(ext)>1):
                ext = ext[1]
            else:
                ext = ''
            if (ext in self.IMG_EXT):
                self.type='img'
                #self.save()
                return 'img'
            elif (ext in self.VID_EXT):
                self.type='vid'
                #self.save()
                return 'vid'
            elif (ext in self.AUD_EXT):
                self.type='aud'
                #self.save()
                return 'aud'
            else:
                self.type='doc'
                #self.save()
                return 'doc'
        self.type='none'
        #self.save()
        return 'none'

    @property
    def to_dict(self):
        out = {
            'type':self.type,
            'id':self.id,
        }
        if not(self.file.name is None):
            out['name']=split(self.file.name)[1]
            out['url']=self.file.url
            try:
                out['preview']=self.preview.url
            except:
                pass
            out['size']=self.file.size
        if not(self.data is None):
            out['data']=self.data
        return out


class ChatMessage (models.Model): #сообщения
    sender_id = models.IntegerField(default=-1)
    reply = models.ForeignKey('self', null=True, on_delete=models.SET_NULL)
    room = models.ForeignKey(Room, null=True, on_delete=models.SET_NULL)
    date = models.DateTimeField()
    message = models.CharField(max_length=250)
    attachment = models.ManyToManyField(Attachment)
    edited = models.SmallIntegerField(default=0)
    is_active = models.SmallIntegerField(default=1)

    def __str__(self):
        return self.message

    @property
    def sender(self):
        return User.objects.filter(id=self.sender_id).first()

    @property
    def date_str(self):
        return self.date.strftime("%Y-%m-%d %H:%M:%S")

    @property
    def to_dict(self):
        repl = {}
        attachments = []
        for att in self.attachment.all():
            if (att.is_active==1):
                attachments.append(att.to_dict)
        if not(self.reply is None):
            if (self.reply.is_active==1):
                repl = {
                    "id":self.reply.id,
                    'username':classes.get_u_name(self.reply.sender_id),
                    "userID":str(self.reply.sender_id),
                    "date":self.reply.date_str,
                    "room":str(self.reply.room.id),
                    "message":self.reply.message,
                    "msg_type":c_set.MSG_TYPE_ARCHIVE,
                }
            else:
                repl = {
                    "id":self.reply.id,
                    'username':'---',
                    "userID":str(self.reply.sender_id),
                    "date":datetime.datetime(2100,1,1).strftime("%Y-%m-%d %H:%M:%S"),
                    "room":str(self.reply.room.id),
                    "message":'Message deleted',
                    "msg_type":c_set.MSG_TYPE_DELETED,
                }
        return {
            'id':self.id,
            'userID':self.sender_id,
            'username':classes.get_u_name(self.sender_id),
            'reply':repl,
            'attachment':attachments,
            'room':self.room.id,
            'message':self.message,
            'date':self.date_str,
            'edited':self.edited,
        }

    @property
    def preview(self):
        attachments = []
        for att in self.attachment.all():
            if (att.is_active==1):
                attachments.append(att.to_dict)
        out={
            'id':self.id,
            'userID':self.sender_id,
            'username':classes.get_u_name(self.sender_id),
            'attachment':attachments,
            'message':self.message,
            'date':self.date_str,
            'edited':self.edited,
        }
        return out

    @property
    def to_dict_light(self):
        out={
            'id':self.id,
            'userID':self.sender_id,
            'username':classes.get_u_name(self.sender_id),
            'attachment':self.attachment.filter(is_active=1).count(),
            'message':self.message,
            'date':self.date_str,
        }
        return out

class Unread (models.Model): #непрочитанные сообщения
    date = models.DateTimeField(default=None, null=True, blank=True)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    user_id = models.IntegerField(default=-1)

    @property
    def date_str(self):
        if not(self.date is None):
            return self.date.strftime("%Y-%m-%d %H:%M:%S")
        else:
            return datetime.datetime(2100,1,1).strftime("%Y-%m-%d %H:%M:%S")

    @property
    def date_or_zero(self):
        if self.date is None:
            return datetime.datetime(2000,1,1)
        return self.date

    @classmethod
    def last_read(self,room,user):
        """
            дата самого нового прочитанного не указанным пользователем сообщения
            если ничего не наашлось возвращает 1 января 2000 года
        """
        date = datetime.datetime(2000,1,1)
        items = self.objects.filter(room_id=room).exclude(user_id=user.id)
        if (len(items)>0):
            for item in items:
                if (item.user_id!=user.id and item.date>date):
                    date=item.date
        return date.strftime("%Y-%m-%d %H:%M:%S")

    @classmethod
    def getcount(self, room, user):
        obj = self.objects.filter(user_id = user.id, room = room).first()
        if (not obj is None):
            date = obj.date
            if date is None:
                date = datetime.datetime(2000,1,1)
            roommes = ChatMessage.objects.filter(room = room.id, date__gt = date, is_active = True).exclude(sender_id = user.id).count()
            return roommes
        else:
            return -1

    @classmethod
    def readed(self, message, user):
        """
        Отмечает факт загрузки пользователем сообщения и если оно
        новее последнего прочитанного пользователем сообщения
        (сравнение осуществляется по ИД сообщения), устанавливает его как
        последнее прочитанное.
        Возвращает True если сообщение новее последнего прочитанного пользователем,
        иначе возвращает False
        """
        #return False
        obj = self.objects.filter(user_id = user.id, room = message.room).first()
        new = False
        if (obj is None):
            obj = self(date = message.date, room = message.room, user_id = user.id)
            obj.save()
            new = True
        elif (obj.date_or_zero < message.date):
            obj.date = message.date
            obj.save()
            new = True
        return(new)

    def __str__(self):
        return (self.message)

class ChatAuth(models.Model):

    uid = models.IntegerField(default=-1)#ИД пользователя чата
    uid_s = models.IntegerField(default=-1)#ИД пользователя СУРа
    key = models.CharField(max_length=50)
    date = models.DateTimeField(blank=True)

    @classmethod
    def clean(self):
        time = datetime.datetime.now(tz = timezone.get_default_timezone())
        self.objects.filter(date__lt = (time - datetime.timedelta(minutes=SESSION_DURATION))).delete()

    def __str__(self):
        return self.uid

# Create your models here.

class color_themes(models.Model):
    """
    Цветовые темы
    """
    name = models.CharField(max_length=100)
    user_id = models.IntegerField()

class color_themes_colors(models.Model):
    """
    Цвета
    """
    name = models.CharField(max_length=100)
    code_name = models.CharField(max_length=100)

class color_themes_color_theme(models.Model):
    """
    Сопостевление значений цветов цветовым темам
    """
    theme = models.ForeignKey(color_themes, on_delete=models.CASCADE)
    color = models.ForeignKey(color_themes_colors, on_delete=models.CASCADE)
    light = models.CharField(max_length=10)
    dark = models.CharField(max_length=10)

class pers_settings(models.Model):
    """
        настройки пользователей
    """
    user_id = models.IntegerField()
    Lang = models.CharField(max_length=6, default='')
    Theme = models.ForeignKey(color_themes, null=True, on_delete=models.SET_NULL)
    Theme_dark = models.BooleanField(default = False)

    def get_settings(user_id):
        if (user_id is None):
            return
        set_obj = pers_settings.objects.filter(user_id = user_id).first()
        if (set_obj is None):
            set_obj = pers_settings.objects.create(user_id = user_id)
            set_obj.save()
        return set_obj
