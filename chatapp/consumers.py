# -*- coding: utf-8 -*-
import json, datetime, celery, redis, psycopg2
from channels import Channel
from django.core import serializers
from django.utils import timezone
from channels.auth import channel_session, channel_and_http_session_user_from_http

from .settings import MSG_TYPE_LEAVE, MSG_TYPE_ENTER, NOTIFY_USERS_ON_ENTER_OR_LEAVE_ROOMS, MSG_TYPE_ARCHIVE, MSG_TYPE_OLDEST, MSG_TYPE_READED
from .models import ChatMessage, Room, Unread, ChatAuth, ChatMembers
from django.contrib.auth.models import User
from .ext_models import sentry_user

from django.core import serializers
from .utils import get_room_or_error, catch_client_error, get_user_obj
from .exceptions import ClientError
from settings_local import BROKER_URL, REDIS_HOST, REDIS_PORT, REDIS_MESSAGE_EXPIRE_TIME, DB_HOST, DB_PORT, DB_USER, DB_PASS, DB_NAME, SUR_DB_HOST, SUR_DB_NAME, SUR_DB_PASS, SUR_DB_PORT, SUR_DB_USER

ARCHIVE_SIZE = 10

from django.http import HttpResponse

import urllib

from channels import Group

USERNAME_TABLE = "sentry_user"

def get_full_name (user):
    """
    берёт ФИО пользователя из sentry_user по его ИД в Auth_user
    если ФИО получить не удалось, берётся username пользователя в auth_user
    """
    res = sentry_user.objects.filter(auth_id = user.id).values_list('full_name', flat=True).first()
    if not(res is None):
        return (res)
    else:
        return (user.username)

def read(mess, user):
    """
        Отмечает факт загрузки сообщения mess пользователем user.
        Если сообщение новее последнего просмотренного, отправляет
        соответствующее уведомление всем окнам этого пользователя где открыт чат
        что бы убрать в них уведомление о новых сообщениях.
    """
    if(Unread.readed(mess, user)):
        final_msg = {'room': str(mess.room.id), 'msg_type': MSG_TYPE_READED}
        Group(str(user.id)).send(
            {"text": json.dumps(final_msg)}
        )

def getmessagesrd(room, con = NotImplemented):
    """
    берёт из ключей редиса все доступные сообщения для чата room
    в параметр con предполагается передавать редис-объект если к моменту
    вызова уже имеется созданный, если он не был передан, функция создаст свой
    """
    if (con == NotImplemented):
        con = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT)
        keys = con.keys('chatmess:'+room+'[[*]*>*')
    else:
        keys = con.keys('chatmess:'+room+'[[*]*>*')
    js=[]
    for key in keys:
        js.append(json.dumps({
            'room':room,
            'text':(con.get(key)),
            'user':key[(key.find('[')+1):key.find(']')],
            'userid':key[(key.find(']')+1):key.find('>')],
            'date':key[(key.find('>')+1):len(key)],
        }))
    return js

def getmessagespg(date, room, amount):
    """
    Берёт сообщения для отдельного чата из базы
    Берётся определённое количество сообщений начиная с определённой даты
    """
    #celeryobj = celery.Celery(broker=BROKER_URL, backend=BROKER_URL)
    #res = celeryobj.send_task("chatapp.tasks.getmessage", kwargs={'chatid' : room, 'time' : date, 'amount' : amount})
    #return serializers.deserialize("json",res.get())
    messages = ChatMessage.objects.order_by("date").filter(room_id = room, date__lt = date, is_active = True).reverse()[:amount]
    return messages

def isallowed(user, room):
    """
        Проверяет, что пользователю разрешён доступ в чат
    """
    obj = ChatMembers.objects.filter(user_id = user.id, chat_id = room.id)
    if (obj.count()>0):
        return True
    else:
        return False    

@channel_and_http_session_user_from_http
def ws_connect(message):
    """
        Если пользователь не авторизирован через джанго (непосредственно в чате, а не в СУРе),
        ищет пользователя среди объектов chatauth по ключу сессии.
        Перед поиском производит очистку от записей старее заданного в настройках времени жизни
        После этого ИД полученного тем или иным спосоом пользователя заносится в параметры 
        channels-сессии как 'user'
    """
    if (not message.user.is_authenticated()):
        ChatAuth.clean()
        cao = ChatAuth.objects
        cao = cao.filter(key = message.http_session._get_session_key())
        user = None
        if (cao.count() > 0):
            user = cao[0].uid
    else:
        user = message.user
    if (user):
        message.reply_channel.send({"accept": True})
        message.channel_session['rooms'] = []
        message.channel_session['user'] = user.id
        Group(str(user.id)).add(message.reply_channel)
        chat_list(message)
    else:
        message.reply_channel.send({"accept": False})

def ws_receive(message):
    payload = json.loads(message['text'])
    payload['reply_channel'] = message.content['reply_channel']
    Channel("chat.receive").send(payload)

@channel_session
def ws_disconnect(message):
    for room_id in message.channel_session.get("rooms", set()):
        try:
            room = get_room_or_error(room_id)
            room.websocket_group.discard(message.reply_channel)
            room.websocket_group_stb.discard(message.reply_channel)
            Group(str(message.channel_session['user'])).discard(message.reply_channel)
        except Room.DoesNotExist:
            pass

@channel_session
def chat_leavejoin(message):
    """
    подключение к другому чату
    вынесено в отдельную функцию так как при вызове со стороны
    клиента последовотельно команд покидния и входа в чат они могли
    выполняться в неверной последовательности
    """
    message ["room"] = message["from"]
    chat_leave(message)

    message ["room"] = message["to"]
    chat_join(message)
    
@channel_session
def chat_join(message):
    timest = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")#текущие время и дата в UTC
    user = get_user_obj(message.channel_session["user"])
    user_full_name = get_full_name(user)
    room = get_room_or_error(message["room"], user)
    if not(isallowed(user, room)):
        return
    if NOTIFY_USERS_ON_ENTER_OR_LEAVE_ROOMS: #уведомление о входе и выходе пользовтелей
        room.send_message(None, user, user_full_name, timest, MSG_TYPE_ENTER)

    room.websocket_group.add(message.reply_channel)
    message.channel_session['rooms'] = list(set(message.channel_session['rooms']).union([room.id]))
    message.reply_channel.send({
        "text": json.dumps({
            "join": str(room.id),
            "title": room.title,
        }),
    })
    r = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT)
    js = getmessagesrd(str(room.id),r)
    jspg = getmessagespg(timest,str(room.id),ARCHIVE_SIZE)
    for i in js:
        mes = json.loads(i) #каждое извлечённое из редиса сообщение отпрвляется по вебсокету
        message.reply_channel.send({
            "text": json.dumps({
                "room":str(room.id),
                "username":user_full_name,
                "userID":str(mes['userid']),
                "date":mes['date'],
                "message":mes['text'],
                "msg_type":MSG_TYPE_ARCHIVE,
            })
        })

    for i in jspg:
        dbmes=i
        user_full_name = get_full_name(dbmes.sender)
        read(dbmes, user)
        dbdate = dbmes.date.strftime("%Y-%m-%d %H:%M:%S")
        Check = True
        for j in js:
            rdmes=json.loads(j)
            if (dbdate==rdmes['date'])and(dbmes.sender.id==rdmes['userid']):
                Check = False
                #checkings
                if (dbmes.message!=rdmes['text'] or dbmes.sender.id!=rdmes['user']): #если текст сообщения или имя пользователя не совпадают
                    r.delete('chatmess:' + rdmes['room'] + '[' + str(rdmes['user']) + ']' + str(rdmes['userid']) + '>' + rdmes['date'])
                    name = 'chatmess:' + dbmes.room.id + '[' + str(dbmes.sender) + ']' + str(dbmes.sender.id) + '>' + dbdate
                    r.set(name, dbmes.message)
                    r.expire(name, REDIS_MESSAGE_EXPIRE_TIME)
                    message.reply_channel.send({
                        "text": json.dumps({
                            "room":str(dbmes.room.id),
                            "username":user_full_name,
                            "userID":str(dbmes.sender.id),
                            "date":dbdate,
                            "message":dbmes.message,
                            "msg_type":MSG_TYPE_ARCHIVE,
                        })
                    })
                break
        if (Check): #если в памяти редиса сообщение найдено не было, оно берётся из базы
            message.reply_channel.send({
                "text": json.dumps({
                    "room":str(dbmes.room.id),
                    "username":user_full_name,
                    "userID":str(dbmes.sender.id),
                    "date":dbdate,
                    "message":dbmes.message,
                    "msg_type":MSG_TYPE_ARCHIVE,
                })
            })


@channel_session
def chat_leave(message):
    user = get_user_obj(message.channel_session["user"])
    user_full_name = get_full_name(user)
    room = get_room_or_error(message["room"], user)

    if NOTIFY_USERS_ON_ENTER_OR_LEAVE_ROOMS:
        room.send_message(None, user, user_full_name, MSG_TYPE_LEAVE)

    room.websocket_group.discard(message.reply_channel)
    message.channel_session['rooms'] = list(set(message.channel_session['rooms']).difference([room.id]))
    message.reply_channel.send({
        "text": json.dumps({
            "leave": str(room.id),
        }),
    })

@channel_session
def chat_getarchive(message):
    jspg = getmessagespg(message["date"],str(message["room"]),ARCHIVE_SIZE)
    cnt = 0
    for i in jspg:
        cnt += 1
        dbmes=i
        dbdate = dbmes.date.strftime("%Y-%m-%d %H:%M:%S")
        user_full_name = get_full_name(dbmes.sender)
        message.reply_channel.send({
            "text": json.dumps({
                "room":str(dbmes.room.id),
                "username":user_full_name,
                "userID":str(dbmes.sender.id),
                "date":dbdate,
                "message":dbmes.message,
                "msg_type":MSG_TYPE_ARCHIVE,
            })
        })
    if jspg.count()<ARCHIVE_SIZE:
        message.reply_channel.send({
            "text": json.dumps({
                "room":str(message['room']),
                "msg_type":MSG_TYPE_OLDEST,
            })
        })

@channel_session
def chat_send(message):

    timest = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") #текущие время и дата в UTC

    if int(message['room']) not in message.channel_session['rooms']:
        raise ClientError("ROOM_ACCESS_DENIED")
    user = get_user_obj(message.channel_session["user"])
    room = get_room_or_error(message["room"], user)
    user_full_name = get_full_name(user)
    room.send_message(message["message"], user, user_full_name, timest)

    r = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT)
    name = 'chatmess:' + message['room'] + '[' + str(user) + ']' + str(user.id) + '>' + timest #добавление сообщения в редис с именем чата, пользователя и датой в качестве ключа
    r.set(name, message['message'])
    r.expire(name, REDIS_MESSAGE_EXPIRE_TIME)


    #вызов задания на добавление сообщения в базу
    sender = get_user_obj(message.channel_session['user'])
    roomobj = get_room_or_error(room.id, sender)
    mes = ChatMessage.objects.create(sender = sender, room = roomobj, date = timest, message = message["message"])
    
    room.new_message()
    read(mes, sender)
    #celeryobj = celery.Celery(broker=BROKER_URL, backend=BROKER_URL)
    #res = celeryobj.send_task("chatapp.tasks.send", kwargs={'message':message["message"],'user':message.user.id, 'room': room.id, 'time': timest})

@channel_session
def chat_list(message):
    """
    список чатов
    """
    #celeryobj = celery.Celery(broker=BROKER_URL, backend=BROKER_URL)
    #res = celeryobj.send_task("chatapp.tasks.roomlist", kwargs={'userid' : str(message.user.id)})
    #rooms = serializers.deserialize("json",res.get())
    user = get_user_obj(message.channel_session["user"])
    rooms = ChatMembers.objects.filter(user = user)
    chatlist = []
    for room in rooms:
        room.chat.websocket_group_stb.add(message.reply_channel)
        lastmes = ChatMessage.objects.order_by("date").filter(room = room.chat, is_active = True).reverse()
        lastread = Unread.objects.filter(user = user, room = room.chat)
        unread = 0
        if (lastmes.count() > 0) and (lastread.count() > 0):
            if not(lastmes[0] == lastread[0].message):
                unread = 1
        chatlist.append({'chat' : room.chat, 'unread' : unread})
    message.reply_channel.send({
        "text":json.dumps({
            "list": 1,
            "rooms":json.dumps([{"id": chat['chat'].pk, "name":chat['chat'].title, "unread": chat['unread']} for chat in chatlist])
            }),
    })