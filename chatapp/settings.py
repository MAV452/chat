from django.conf import settings

NOTIFY_USERS_ON_ENTER_OR_LEAVE_ROOMS = False

MSG_TYPE_DELETED = -5  # Deleted message
MSG_TYPE_NEWEST = -4  # Reached newest messages
MSG_TYPE_NO_MES = -3  # No messages in this chat
MSG_TYPE_OLDEST = -2  # End of history mark
MSG_TYPE_ARCHIVE = -1  # For archive messages
MSG_TYPE_MESSAGE = 0  # For standard messages
MSG_TYPE_WARNING = 1  # For yellow messages
MSG_TYPE_ALERT = 2  # For red & dangerous alerts
MSG_TYPE_MUTED = 3  # For just OK information that doesn't bother users
MSG_TYPE_ENTER = 4  # For just OK information that doesn't bother users
MSG_TYPE_LEAVE = 5  # For just OK information that doesn't bother users
MSG_TYPE_NEW = 6    # New message in some of the chats user have access to
MSG_TYPE_READED = 7 # User readed new message from chat

MESSAGE_TYPES_CHOICES = getattr(settings, 'MESSAGE_TYPES_CHOICES', (
    (MSG_TYPE_DELETED, 'DELETED'),
    (MSG_TYPE_NEWEST, 'NEWEST'),
    (MSG_TYPE_NO_MES, 'NO_MES'),
    (MSG_TYPE_OLDEST, 'OLDEST'),
    (MSG_TYPE_ARCHIVE, 'ARCHIVE'),
    (MSG_TYPE_MESSAGE, 'MESSAGE'),
    (MSG_TYPE_WARNING, 'WARNING'),
    (MSG_TYPE_ALERT, 'ALERT'),
    (MSG_TYPE_MUTED, 'MUTED'),
    (MSG_TYPE_ENTER, 'ENTER'),
    (MSG_TYPE_LEAVE, 'LEAVE'),
    (MSG_TYPE_NEW, 'NEW'),
    (MSG_TYPE_READED, 'READED')))

MESSAGE_TYPES_LIST = getattr(settings, 'MESSAGE_TYPES_LIST',
                             [MSG_TYPE_DELETED,
                              MSG_TYPE_NEWEST,
                              MSG_TYPE_NO_MES,
                              MSG_TYPE_OLDEST,
                              MSG_TYPE_ARCHIVE,
                              MSG_TYPE_MESSAGE,
                              MSG_TYPE_WARNING,
                              MSG_TYPE_ALERT,
                              MSG_TYPE_MUTED,
                              MSG_TYPE_ENTER,
                              MSG_TYPE_LEAVE,
                              MSG_TYPE_NEW,
                              MSG_TYPE_READED])