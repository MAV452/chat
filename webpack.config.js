var path = require('path');
var webpack = require('webpack');
var django_webpack_lib = require('./django_webpack_lib');

const VueLoaderPlugin = require('vue-loader/lib/plugin'); // плагин для загрузки кода Vue
 

const django_conf = {
	apps: ['chatapp'],
	dir_name: 'vues',
	static_subdir: 'vjs',
	app_basedir: "chat",
	static_first: true,
	ext_convert: {"coffee":"js", "styl":"css.js"}
}


module.exports = {
	entry: {
	},
	output: {
		path: path.resolve(__dirname, 'static'),
		publicPath: '/static/',
		//filename: 'build.js',
		//filename: '/static/[name]/js/bundle.js', 
		filename: "",
		libraryTarget:'window',
	},
	optimization:{
		moduleIds: 'hashed',
		//runtimeChunk: 'single',
		splitChunks:{
			cacheGroups:{
				vendor:{
					test: /[\\/]node_modules[\\/]/,
					name:'vendors',
					chunks:'all',
				}
			}
		},
	},
	module: {
		rules: [
			{
				test: /\.s(c|a)ss$/,
				use: [
				  	'vue-style-loader',
					'css-loader',
					{
						loader: 'sass-loader',
					// Requires sass-loader@^8.0.0
						options: {
							implementation: require('sass'),
							sassOptions: {
								fiber: require('fibers'),
								indentedSyntax: true // optional
							},
						},
					},
				],
			},
			{
				test: /\.vue$/,
				loader: 'vue-loader'
			}, {
				test: /\.css$/,
				use: [
					'vue-style-loader',
					'css-loader'
				]
			}, {
				test: /\.coffee$/,
/*				use: [
					{
					loader: 'coffee-loader',
					options: { 
						transpile: {
						presets: ['env']
						}
					}
					}
				]
*/
				use: [
					'coffee-loader',
				]
			}, {
				test: /\.styl$/,
				use: [
					"style-loader",
					"css-loader",
					{
						loader: "stylus-loader", // compiles Stylus to CSS
					 	options: {
					 		preferPathResolver: 'webpack'
						},
					}
				]
			},
			// загрузчики для пакета иконок
			{ 
				test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
				loader: "url-loader?limit=10000&mimetype=application/font-woff",
				options:{
					resourcePath: '',
				}
			},
			{ 
				test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
				loader: "file-loader",
				options:{
					outputPath: '',
				}
			}
		],
	},
	resolve: {
		modules: [
			"./node_modules/"
		  ],
		  extensions: [".js", ".json", ".css", ".jsx"]
	},
	mode: 'development',
	//mode: 'production',
	externals: [
		{
			vue: 'Vue',
			vuex: 'Vuex',      
			axios: 'axios',
			store: 'store',
			vuerouter: 'VueRouter',
		}
	],
	plugins: [
		new VueLoaderPlugin(),
	]
}

django_webpack_lib.setup_webpack(module.exports, django_conf)
