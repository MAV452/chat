function ddigit(digit){ //добавление ведущего ноля к однозначным числам
    if (digit < 10) {
        return '0'+String(digit);
    } else {
        return String(digit);
    }
}

function tolocal(date){//приведение даты к текущему часовому поясу
    d = new Date(date);
    d.setMinutes(d.getMinutes()-(new Date().getTimezoneOffset()));
    str = ddigit(d.getDate()) + '-' + ddigit(d.getMonth() + 1) + '-' + d.getFullYear() + ' ' + ddigit(d.getHours()) + ':' + ddigit(d.getMinutes()) + ':' + ddigit(d.getSeconds());
    return str;
}

function mestype(data){//преобразование сообщения в html код в соответсвии с типом
    switch (data.msg_type) {
        case (-2):
            return "<div class='oldest' >End of history</div>"
            break;
        case (-1):
        case (0):
            // Message
            return "<div class='message' date='"+ data.date +"'>" +
                "<span class='username'>" + tolocal(data.date) + " </span>" +
                "<span class='username'>" + data.username + " </span>" +
                "<span class='body'>" + data.message + "</span>" +
                "</div>";
            break;
        case 1:
            // Warning/Advice messages
            return "<div class='contextual-message text-warning'>" + data.message + "</div>";
            break;
        case 2:
            // Alert/Danger messages
            return "<div class='contextual-message text-danger'>" + data.message + "</div>";
            break;
        case 3:
            // "Muted" messages
            return "<div class='contextual-message text-muted'>" + data.message + "</div>";
            break;
        case 4:
            // User joined room
            return "<div class='contextual-message text-muted'>" + data.username + " joined the room!" + "</div>";
            break;
        case 5:
            // User left room
            return "<div class='contextual-message text-muted'>" + data.username + " left the room!" + "</div>";
            break;
        default:
            console.log("Unsupported message type!");
            return;
    }

}
// Says if we joined a room or not by if there's a div for it
function inRoom() {
    return $(".room").length > 0;
};

const config = {
    root: document.querySelector('#vroom'),
    rootMargin: '5px',
    threshold: [0,1]
};

var updateblock = true;

$(function () {
    // Correctly decide between ws:// and wss://
    var ws_scheme = window.location.protocol == "https:" ? "wss" : "ws";
    var ws_path = ws_scheme + '://' + window.location.host + "/chat/stream/";
    console.log("Connecting to " + ws_path);
    var socket = new WebSocket(ws_path);

    VRooms = new Vue({//список чатов
        el: '#vrlist',
        delimiters: ['[[',']]'],
        data: {
        rooms: [
            ]
        },
        methods:{
            click:function(id){      
            roomId = id;
            console.log(id);
            if (inRoom()) { //если мы в чате
                curroom = $(".room").attr("id")
                console.log(curroom);
                if(curroom == roomId){//если мы уже в чате в который собираемся войти то ничего не делаем
                    return;
                }
                if(roomId == -1){//если была нажата ссылка выхода из чата
                    //  покидаем текущий чат
                    $(this).removeClass("joined");
                    socket.send(JSON.stringify({ 
                        "command": "leave",  
                        "room": curroom
                    }));
                }else{
                    // иначе входим в новый чат
                    $(this).removeClass("joined");
                    socket.send(JSON.stringify({ 
                        "command": "ljoin",  
                        "from": curroom,
                        "to":id
                    }));
                    $(this).addClass("joined");
                }
            } else {
                // если мы не находимся в данный момент в другом чате то просто входим в чат
                $(this).addClass("joined");
                socket.send(JSON.stringify({
                    "command": "join",
                    "room": roomId
                }));
            }
        }
    }
    })


    // Helpful debugging
    socket.onopen = function () {
        console.log("Connected to chat socket");
    };
    socket.onclose = function () {
        console.log("Disconnected from chat socket");
    };

    socket.onmessage = function (message) {
        // Decode the JSON
        console.log("Got websocket message " + message.data);
        var data = JSON.parse(message.data);
        // Handle errors
        if (data.error) {
            alert(data.error);
            return;
        }
        if (data.list) {

            var rooms = eval('(' + data.rooms + ')');
            for (i in rooms){
                VRooms.rooms.push(
                    {
                        'name': rooms[i]["name"], 
                        'id': rooms[i]["id"]
                    }
                )
            }
            //заготовка для обработчика получения сиска чатов
            console.log("List of rooms " + JSON.stringify(data.rooms));
        }
        // Handle joining
        if (data.join) {
            console.log("Joining room " + data.join);
            var roomdiv = $(
                "<div class='room' id='" + data.join + "'>" +
                "<h2>" + data.title + "</h2>" +
                "<div id='vroom' style='overflow: scroll; height:300px;'>" + 
                "<div id='top'></div>" +
                "<div v-for='x in messages' class='v.pos' v-html='x.html'> </div>" +
                "<div id='bottom'></div>" + 
                "</div>" +
                "<input><button>Send</button>" +
                "</div>"
            );
            $("#chats").append(roomdiv);
            var oldest = true;
            updateblock = false;
            VMess = new Vue({ // список сообщений чата
                el: '#vroom',
                delimiters: ['[[',']]'],
                data: {
                messages: [
                    ]
                },
                methods: {
                    data(){ 
                        return {
                            observer: null
                        }
                    },
                    even: function(arr) {//сортировка по дате
                      return arr.slice().sort(function(a, b) {
                        return new Date(a.date) - new Date(b.date);
                      });
                    },
                  },
                mounted(){
                    this.observer = new IntersectionObserver(
                        function callback (items) {
                            if(updateblock){
                                for (i=0; i<items.length; i++){
                                    if (items[i].isIntersecting){
                                        if (oldest){ //если не достигнут конец истории
                                            if(items[i].target.getAttribute('id') == 'top'){
                                                console.log('intersect top');
                                                updateblock = false;
                                                socket.send(JSON.stringify({ 
                                                    "command": "getarchive",  
                                                    "room": $(".room").attr("id"),
                                                    "date": VMess.messages[0].date,
                                                    "dir":1
                                                }));
                                            }
                                        }
                                        if(items[i].target.getAttribute('id') == 'bottom'){
                                            console.log('intersect bottom');
                                            //updateblock = false;
                                            //socket.send(JSON.stringify({ 
                                            //    "command": "getarchive",  
                                            //    "room": $(".room").attr("id"),
                                            //    "date": VMess.messages[VMess.messages.length-1].date,
                                            //    "dir":0
                                            //}));
                                        }
                                    }
                                }
                            };
                        }
                        , config);
                    this.observer.observe(document.querySelector('#top'));
                    this.observer.observe(document.querySelector('#bottom'));
                },
                destroyed(){
                    this.observer.disconnect();
                },
            })
            roomdiv.find("button").on("click", function () { //нажатие кнопки отправления
                socket.send(JSON.stringify({
                    "command": "send",
                    "room": data.join,
                    "message": roomdiv.find("input").val()
                }));
                roomdiv.find("input").val("");
            });
            // Handle leaving
        } else if (data.leave) {
            console.log("Leaving room " + data.leave);
            $("#"+data.leave).remove();
            VMess.delete;
        } else if (data.msg_type == -2) {
            if (VMess.messages[0].msg_type != -2){
                VMess.messages.splice(0, 0,
                    {
                        'user': '',
                        'userID': 0, 
                        'date': new Date(0),
                        'message': '',
                        'msg_type': -2,
                        'pos':'',
                        'html': mestype(data) //сообщение в виде html кода
                    }
                );
            }
            oldest = false;
        } else if (data.message || data.msg_type != 0) {
            var msgdiv = $(".room #vroom");
            if (typeof VMess != "undefined"){
                var mesCheck = VMess.messages.filter(function(elem){ //если есть сообщение от этого пользователя на это время обновляет его текст вместо дублирования
                    if (elem.userID == data.userID && elem.date == data.date){
                        if(elem.message != data.message || elem.user != data.username){
                            Vue.set(elem, 'message', data.message);
                            Vue.set(elem, 'user', data.user);
                            Vue.set(elem, 'html', mestype(data));
                        }
                        return 'dub';
                    }
                });
                updateblock = true;
                if (mesCheck.length == 0){
                    var i = 0;
                    var pos = '';
                    mesdate = new Date(data.date)
                    if(VMess.messages.length>0){ //находим индекс первого элемента с датой больше добавляемого
                        while ((i < VMess.messages.length) && (new Date(VMess.messages[i].date) < mesdate)){
                            i = i + 1;
                        }
                    }
                    //и вставляем элемент перед ним
                    VMess.messages.splice(i, 0,
                        {
                            'user': data.username,
                            'userID': data.userID, 
                            'date': data.date,
                            'message': data.message,
                            'msg_type': data.msg_type,
                            'pos':pos,
                            'html': mestype(data) //сообщение в виде html кода
                        }
                    );
                }
            }
            var ok_msg = mestype(data);
            msgdiv.scrollTop(msgdiv.prop("scrollHeight"));
        } else {
            console.log("Cannot handle message!");
        }
    };

    // Says if we joined a room or not by if there's a div for it
    function inRoom() {
        return $(".room").length > 0;
    };

    // Room join/leave
    $(".room-link").click(function () { //нажатие на кнопку входа в чат
        console.log('click');
        roomId = $(this).attr("data-room-id");
        if (inRoom()) { //если мы в чате
            curroom = $(".room").attr("id")
            if(curroom == roomId){//если мы уже в чате в который собираемся войти то ничего не делаем
                return;
            }
            // иначе покидаем текущий чат
            $(this).removeClass("joined");
            socket.send(JSON.stringify({ 
                "command": "leave",  
                "room": curroom
            }));
            if(roomId != -1){//если была нажата не ссылка выхода из чата
                // входим в новый чат
                $(this).addClass("joined");
                socket.send(JSON.stringify({
                    "command": "join",
                    "room": roomId
                }));
            }
        } else {
            // если мы не находимся в данный момент в другом чате то просто входим в чат
            $(this).addClass("joined");
            socket.send(JSON.stringify({
                "command": "join",
                "room": roomId
            }));
        }
    });
});