(function(e, a) { for(var i in a) e[i] = a[i]; }(window, /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/static/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "0xzq");
/******/ })
/************************************************************************/
/******/ ({

/***/ "0xzq":
/*!********************************************!*\
  !*** ./chatapp/vues/chatapp/base_mixin.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* */\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n    data(){\n        return{\n            loading:false,\n            //флаг отобрвжения колеса загрузки\n            theme_set:false,\n            s_bar:false,\n            message:'',\n            timeout:5000,\n            s_color:'info',\n            //параметры для всплывающей панели сообщений\n            date_format_options:{\n                year: 'numeric',\n                month: '2-digit',\n                day: '2-digit',\n            },\n            time_format_options:{\n                hour: '2-digit',\n                minute: '2-digit',\n                second: '2-digit',\n            },\n            rst:true,\n            loaded:false,\n            u_notif:[],\n            //флаг определяющий загружены ли данные модуля (используется у раскрываемых модулей при их открытии)\n        }\n    },\n    computed:{\n        host(){\n            if (typeof(chaturl) != 'undefined'){\n                return chaturl\n            }\n            return window.location.host+'';\n        },\n        host_or_empty(){\n            if (typeof(chaturl) != 'undefined'){\n                return 'https://'+chaturl\n            }\n            return ''\n        },\n        is_mobile(){\n            //return true;\n            return (typeof window.orientation !== 'undefined');\n        },\n        m_class(){\n            if (this.is_mobile){\n                return 'mobile'\n            }\n            return ''\n        },\n        menu_btn_size(){\n            return ''\n            if (this.is_mobile){\n                return Number(getComputedStyle(document.documentElement).getPropertyValue('--menu-btn-size').replace('px',''));\n            }\n            return ''\n        },\n        btn_size(){\n            return ''\n            if (this.is_mobile){\n                return Number(getComputedStyle(document.documentElement).getPropertyValue('--btn-size').replace('px',''));\n            }\n            return ''\n        },\n        btn_size_tree(){\n            return ''\n            if (this.is_mobile){\n                return Number(getComputedStyle(document.documentElement).getPropertyValue('--btn-size').replace('px',''))*1.5;\n            }\n            return ''\n        },\n        icon_size(){\n            return ''\n            if (this.is_mobile){\n                return Number(getComputedStyle(document.documentElement).getPropertyValue('--icon-size').replace('px',''));\n            };\n            return ''\n        },\n        iconbtnw_size(){\n            return ''\n            if (this.is_mobile){\n                return this.icon_size*1.1;\n            };\n            return ''\n        },\n        iconbtn_size(){\n            return ''\n            if (this.is_mobile){\n                return Number(getComputedStyle(document.documentElement).getPropertyValue('--iconbtn-size').replace('px',''));\n            };\n            return ''\n        },\n        label_font(){\n            return ''\n            if (this.is_mobile){\n                return Number(getComputedStyle(document.documentElement).getPropertyValue('--label-font').replace('px',''));\n            };\n            return ''\n        },\n        header_font(){\n            return ''\n            if (this.is_mobile){\n                return Number(getComputedStyle(document.documentElement).getPropertyValue('--header-font').replace('px',''));\n            };\n            return ''\n        },\n        regular_font(){\n            return ''\n            if (this.is_mobile){\n                return Number(getComputedStyle(document.documentElement).getPropertyValue('--regular-font').replace('px',''));\n            };\n            return ''\n        },\n        sub_font(){\n            return ''\n            if (this.is_mobile){\n                return Number(getComputedStyle(document.documentElement).getPropertyValue('--sub-font').replace('px',''));\n            };\n            return ''\n        },\n        scrollbar_color(){\n            return this.get_color('scrollbar') + ' ' + this.get_color('scrollbar_bg');\n        },\n        app_height(){\n            return window.innerHeight*0.8;\n        },\n        app_width(){\n            return window.innerWidth;\n        },\n        snackbar_offset(){\n            return 0;\n            if(this.is_mobile){\n                return 100;\n            }\n            return 0;\n        },\n        cur_user_name(){\n            return this.$store.getters.get_context_item('u_name');\n        },\n        version(){\n            return this.$store.getters.get_context_item('ver');\n        },\n        is_dark_theme(){\n            return this.$vuetify.theme.dark;\n        },\n        rst_g(){\n            return this.$store.getters.get_rst_g;\n        },\n    },\n    methods:{\n        //функция gettext\n        _(item){\n            //console.log('a')\n            /*if (window.gettext){\n                return window.gettext(item);    \n            }\n            return item;*/\n            if (this.$store){\n                return this.$store.getters.get_text(item);\n            }\n            return item;\n        },\n        //перерендер отдельного компонента\n        reset(){\n            Vue.set(this,'rst',false);\n            var ths = this;\n            Vue.nextTick(function(){Vue.set(ths,'rst',true);});\n        },\n        //заставляет всё перерендерить выключая и включая компоненты через v-if в их корневых div\n        //используется для обновления всего текста при смене языка\n        reset_g(){\n            this.$store.commit('reset_g');\n        },\n        update(){\n            this.$forceUpdate;\n        },\n        int_to_array(item){\n            var out = [];\n            out.push(item);\n            return out;\n        },\n        //загрузка нового языка при его смене\n        lang_reset(code=undefined){\n            var script = document.getElementById('js_catalog');\n            var script_n = document.createElement('script');\n            script_n.id = script.id;\n            var src = script.src;\n            var p = src.indexOf('p?lang=');//удаляет параметр языка если есть\n            if (p!=-1){\n                src = src.substr(0,p);\n            }\n            if (code==undefined){//параметр не указан, определится сервером\n                script_n.src = src;\n            }else{//параметр указан при вызове функции \n                script_n.src = src+'p?lang='+code;\n            }\n            script_n.type = script.type;\n            var ths=this;\n            script_n.onload=function(){ths.$store.commit('drop_gtxt');};\n            script.remove();\n            delete(window.django);\n            var head = document.getElementsByTagName('head')[0];\n            head.appendChild(script_n);\n        },\n        //проверка прав\n        permited(item){\n            return this.$store.getters.is_permited(item);\n        },\n        date_to_str(date){\n            if (date==undefined){\n                return ''\n            }\n            if (!typeof date=='string' || date.length==0){\n                return ''\n            }\n            var dt = new Date(date);\n            return dt.toLocaleString(\"ru\",this.date_format_options);\n        },\n        time_to_str(date){\n            if (date==undefined){\n                return ''\n            }\n            if (!typeof date=='string' || date.length==0){\n                return ''\n            }\n            var dt = new Date(date);\n            return dt.toLocaleString(\"ru\",this.time_format_options);\n        },\n        datetime_to_str(date){\n            if (date==undefined){\n                return ''\n            }\n            if (!typeof date=='string' || date.length==0){\n                return ''\n            }\n            var dt = new Date(date);\n            return dt.toLocaleString(\"ru\",this.time_format_options) + ' ' + dt.toLocaleString(\"ru\",this.date_format_options);\n        },\n        //обработчик ошибок\n        err_process(error){\n            Vue.set(this,'loading',false);\n            var c_name = this.$options._componentTag;\n            if (c_name){\n                this.err_mes(c_name+': '+error.message);\n            }else{\n                this.err_mes(error.message);\n            }\n            console.log(error);\n        },\n        //показать сообщение об ошибке\n        err_mes(message){\n            if(message=='No auth'){\n                location.reload();\n            }\n            Vue.set(this,'s_color','error');\n            Vue.set(this,'message',message);\n            Vue.set(this,'s_bar',true);\n            Vue.set(this,'loading',false);\n            console.log(message);\n        },\n        err_mes_g(message){\n            if(message=='No auth'){\n                location.reload();\n            }\n            this.$store.commit('err_mes',message)\n            console.log(message);\n        },\n        //показать сообщение об успешном выполнении действия\n        suc_mes(message){\n            Vue.set(this,'s_color','success');\n            Vue.set(this,'message',message);\n            Vue.set(this,'s_bar',true);\n            Vue.set(this,'loading',false);\n        },\n        //показать сообщение\n        info_mes(message){\n            Vue.set(this,'s_color','info');\n            Vue.set(this,'message',message);\n            Vue.set(this,'s_bar',true);\n            Vue.set(this,'loading',false);\n        },\n        //получение цвета текущей цветовой темы по его идентификатору\n        get_color(item){\n            if (this.$vuetify.theme.dark){\n                if (this.$vuetify.theme.themes.dark[item]){\n                    return this.$vuetify.theme.themes.dark[item];\n                }\n            }else{\n                if (this.$vuetify.theme.themes.light[item]){\n                    return this.$vuetify.theme.themes.light[item];\n                }\n            }\n            return '';\n        },\n        set_theme(theme, n_r=false){\n            for (var color in theme.light){\n                Vue.set(this.$vuetify.theme.themes.light,color,theme.light[color].value);\n            }\n            for (var color in theme.dark){\n                Vue.set(this.$vuetify.theme.themes.dark,color,theme.dark[color].value);\n            }\n            if (n_r){\n                Vue.set(this,'theme_set',true);\n            }\n            var ths = this;\n            Vue.nextTick(function(){\n                Vue.nextTick(function(){\n                    ths.highlight_css_init()\n                })\n            })\n        },\n        //получить и установить текущую тему пользователя\n        get_cur_theme(){\n            if (!this.permited('themes')){\n                return;\n            }\n            var ths=this;\n            if (!this.theme_set){\n                var context = this.$store.getters.get_context_item('theme');\n                if (context.length>0){\n                    var theme = JSON.parse(context);\n                    Vue.set(ths.$vuetify.theme,'dark',theme.dark);\n                    ths.set_theme(theme.list[0]);\n                }\n            }\n            var qs = ''\n                if (typeof sessionid !='undefined'){\n                    qs=\"&sessionid=\"+sessionid\n                }\n            axios.get(this.host_or_empty+'/list/themes/?cur'+qs, {withCredentials: true,}).then(function(result){\n                if (result.data.error){\n                    console.log(result.data.error);\n                }else{\n                    if (!(result.data.list&&result.data.list[0])){\n                        return;\n                    }\n                    Vue.set(ths.$vuetify.theme,'dark',result.data.dark);\n                    ths.set_theme(result.data.list[0]);\n                }\n            }).catch(error=>{ths.err_process(error);});\n        },\n        u_notif_close(item){\n            var ind = this.u_notif.indexOf(item);\n            if (ind>-1){\n                this.u_notif.splice(ind,1);\n            }\n        },\n        add_u_notif(item){\n            var ind = -1;\n            for (var i in this.u_notif){\n                if (this.u_notif[i].text==item.text){\n                    ind=i;\n                }\n            }\n            if (ind==-1){\n                this.u_notif.push(item);\n            }\n        },\n        on_logout(nc=false, all=false){\n            if (nc || confirm(this._('Log out?'))){\n                var ths = this;\n                var qs = ''\n                if (all){\n                    qs='?all=true'\n                }\n                if (typeof sessionid !=='undefined'){\n                    if (qs.length>0){\n                        qs+='&'\n                    }else{\n                        qs+='?'\n                    }\n                    qs+=\"sessionid=\"+sessionid\n                }\n                axios.get(this.host_or_empty+'/signout/'+qs, {withCredentials: true,}).then(function(){ths.$emit('logout')}).catch(error=>{ths.err_process(error);});\n            }\n        },\n        to_root(){\n            document.location.href=\"/\";\n        },\n        highlight_css_init(){\n            var ssheet = undefined;\n            var flag = false;\n            for(var i in document.styleSheets){\n                ssheet = document.styleSheets[i];\n                var rules = []\n                try{\n                    rules = (ssheet.cssRules || ssheet.rules)//Нельзя получить правила стиля,полученного не с того же сервера\n                } catch(e){\n                    //пропускаем такие стили\n                }\n                for (var j in rules){\n                    if (rules[j].selectorText == '.chatdiv_main .mesdiv.highlight'){\n                        flag = true;\n                        break;\n                    }\n                }\n                if (flag){\n                    break;\n                }\n            }\n            if (!flag){\n                return;\n            }\n\n            var styles = {\n                'point':this.get_color('font_main'),\n                'error':this.get_color('error'),\n            }\n            for (var i in styles){\n                var rules = []\n                try{\n                    rules = (ssheet.cssRules || ssheet.rules)\n                } catch(e){\n                    \n                }\n                var i1,i2=-1;\n                for (var j in rules){\n                    if (rules[j].selectorText == \".chatdiv_main .mesdiv.highlight.\"+i){\n                        i1 = j;\n                    }\n                    if (rules[j].selectorText == \"@keyframes highlight_flickr_\"+i){\n                        i2 = j;\n                    }\n                    if (i1>-1 && i2>-1){\n                        break;\n                    }\n                }\n                if (i1>-1){\n                    ssheet.deleteRule(i1)\n                }\n                if (i2>-1){\n                    ssheet.deleteRule(i2)\n                }\n                var style = \".chatdiv_main .mesdiv.highlight.\"+i+\" {\"\n                style+=\"animation: highlight_flickr_\"+i+\" 4s 3;\"\n                style+=\"background-color: \"+styles[i]+';'\n                style+=\"}\"\n                ssheet.insertRule(style, rules.length);\n                var animation = \"@keyframes highlight_flickr_\"+i+\" {\"\n                animation += \"0% {background-color: \"+styles[i]+\"ff;}\"\n                animation += \"50% {background-color: \"+styles[i]+\"00;}\"\n                animation += \"100% {background-color: \"+styles[i]+\"ff;}\"\n                animation += \"}\"\n                ssheet.insertRule(animation, rules.length);\n            }\n        }\n    }\n});\n\n//# sourceURL=webpack:///./chatapp/vues/chatapp/base_mixin.js?");

/***/ })

/******/ })));