(function(e, a) { for(var i in a) e[i] = a[i]; }(window, /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/static/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "elRy");
/******/ })
/************************************************************************/
/******/ ({

/***/ "WIBD":
/*!***********************!*\
  !*** external "Vuex" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("(function() { module.exports = window[\"Vuex\"]; }());\n\n//# sourceURL=webpack:///external_%22Vuex%22?");

/***/ }),

/***/ "elRy":
/*!***************************************!*\
  !*** ./chatapp/vues/chatapp/store.js ***!
  \***************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ \"i7/w\");\n/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ \"WIBD\");\n/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vuex__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! axios */ \"zr5I\");\n/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_2__);\n\n\n\nvue__WEBPACK_IMPORTED_MODULE_0___default.a.use(vuex__WEBPACK_IMPORTED_MODULE_1___default.a);\nwindow.store = new vuex__WEBPACK_IMPORTED_MODULE_1___default.a.Store({\n    state: {\n      interval:null,\n      permissions: [],\n      context:{},\n      gtxt:{},\n      rst_g:true,\n      errors:[],\n      played:[],\n      player_url:{},\n      ws_state:3,\n      connected:false,\n    },\n    mutations: {\n      //глобальный перерендер\n      reset_g(state){\n        state.rst_g=false;\n        vue__WEBPACK_IMPORTED_MODULE_0___default.a.nextTick(function(){state.rst_g=true;});\n      },\n      err_mes(state,message){\n        state.errors.push(message);\n      },\n      err_after_pull(state,message){\n        state.errors.splice(0,1);\n      },\n      //сброс списка значений для gettext\n      drop_gtxt(state){\n        Object.getOwnPropertyNames(state.gtxt).forEach(function (prop) {\n          delete state.gtxt[prop];\n        });\n        vue__WEBPACK_IMPORTED_MODULE_0___default.a.nextTick(this._mutations.reset_g[0](state));\n      },\n      //изъятие данных передаваемых с формированием шаблона через скрытый div\n      set_context(state,context){\n        for (var item in context){\n          state.context[context[item].name] = context[item].value;\n        }\n      },\n      //задать список прав\n      permition_update (state,vals){\n        state.permissions=vals;\n        console.log('b')\n      },\n      played_add(state,obj){\n          if (state.played.indexOf(obj)==-1){\n            state.played.push(obj);\n          }\n      },\n      played_remove (state,obj){\n        var ind = state.played.indexOf(obj);\n        if (state.played.indexOf(obj)>-1){\n          state.played.slice(ind,1);\n        }\n      },\n      played_stop(state,obj){\n\n      },\n      played_close(state,obj){\n\n      },\n      player_set(state,url){\n        state.player_url = url;\n        console.log(state.player_url)\n      },\n      player_clear(state){\n        state.player_url = {};\n      },\n      set_ws_state(state,val){\n        state.ws_state = val;\n      },\n      set_connected(state,val){\n        state.connected = val;\n      },\n    },\n    actions:{\n\n    },\n    getters: {\n      get_text: state => item => {\n        if (state.gtxt[item]){\n          return state.gtxt[item]\n        }else{\n          if (window.django && window.django.catalog[item]){\n            state.gtxt[item]=window.django.catalog[item]\n            return state.gtxt[item];\n          }else{\n            return item;\n          }\n        }\n        return item;\n      },\n      get_ws_state: state => {return state.ws_state},\n      get_connected: state => {return state.connected},\n      get_played: state => {return state.played},\n      get_player_url: state => {return state.player_url},\n      get_errors: state => {return state.errors},\n      pull_error: state => {if (state.errors.length>0){return state.errors[0]};return undefined;},\n      get_rst_g: state => {return state.rst_g;},\n      //данные передаваемые с сервера при создании страницы\n      get_context_item: state => item => {return state.context[item]},\n      is_permited: state => item => {return state.permissions.indexOf(item)!=-1},\n    }\n  });\n\nfunction host_or_empty(){\n  if (typeof(chaturl) != 'undefined'){\n      return 'https://'+chaturl\n  }\n  return ''\n}\nconst get_perm = async () => {\n  try {\n      var qs = ''\n        if (typeof sessionid !=='undefined'){\n            qs=\"?sessionid=\"+sessionid\n        }\n      const resp = axios__WEBPACK_IMPORTED_MODULE_2___default.a.get(host_or_empty()+'/list/permissions/'+qs, {withCredentials: true,}).then(function(result){\n        if (result.data.error){\n          console.log(error)\n          alert(error);\n        }else{\n          console.log('a')\n          window.store.commit('permition_update', result.data.list);\n        }\n      });\n  } catch (err) {\n      // Handle Error Here\n      console.error(err);\n  }\n};\n\n\nget_perm();\n\nconsole.log('c')\nwindow.addEventListener(\"load\",function(event) {\n  if (typeof(chat_context_obj)!='undefined'){\n    var attr = chat_context_obj;\n    for (var i in attr){\n      attr[i]={'name':i,'value':attr[i]}\n    }\n  }else{\n    var c_div = document.getElementById(\"chat_context\");\n    var attr=c_div.attributes;\n  }\n  window.store.commit('set_context', attr);\n},false);\n\n//# sourceURL=webpack:///./chatapp/vues/chatapp/store.js?");

/***/ }),

/***/ "i7/w":
/*!**********************!*\
  !*** external "Vue" ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("(function() { module.exports = window[\"Vue\"]; }());\n\n//# sourceURL=webpack:///external_%22Vue%22?");

/***/ }),

/***/ "zr5I":
/*!************************!*\
  !*** external "axios" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("(function() { module.exports = window[\"axios\"]; }());\n\n//# sourceURL=webpack:///external_%22axios%22?");

/***/ })

/******/ })));