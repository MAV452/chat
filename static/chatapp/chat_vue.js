function ddigit(digit){ //добавление ведущего ноля к однозначным числам
    if (digit < 10) {
        return '0'+String(digit);
    } else {
        return String(digit);
    }
}

function tolocal(date){//приведение даты к текущему часовому поясу
    d = new Date(date);
    d.setMinutes(d.getMinutes()-(new Date().getTimezoneOffset()));
    str = ddigit(d.getDate()) + '-' + ddigit(d.getMonth() + 1) + '-' + d.getFullYear() + ' ' + ddigit(d.getHours()) + ':' + ddigit(d.getMinutes()) + ':' + ddigit(d.getSeconds());
    return str;
}

function mestype(data){//преобразование сообщения в html код в соответсвии с типом
    switch (data.msg_type) {
        case (-2):
            return "<div class='oldest' id='oldest' >Конец истории</div>"
            break;
        case (-1):
        case (0):
            // Message
            return "<div class='message' date='"+ data.date +"'>" +
                "<div class='username'>" + data.username + " </div>" +
                "<div class='body'>" + data.message + "</div>" +
                "<div class='date'>" + tolocal(data.date) + " </div>" +
                "</div>";
            break;
        case 1:
            // Warning/Advice messages
            return "<div class='contextual-message text-warning'>" + data.message + "</div>";
            break;
        case 2:
            // Alert/Danger messages
            return "<div class='contextual-message text-danger'>" + data.message + "</div>";
            break;
        case 3:
            // "Muted" messages
            return "<div class='contextual-message text-muted'>" + data.message + "</div>";
            break;
        case 4:
            // User joined room
            return "<div class='contextual-message text-muted'>" + data.username + " joined the room!" + "</div>";
            break;
        case 5:
            // User left room
            return "<div class='contextual-message text-muted'>" + data.username + " left the room!" + "</div>";
            break;
        default:
            console.log("Unsupported message type!");
            return;
    }

}


const config = {
    root: document.querySelector('#vroom'),
    rootMargin: '5px',
    threshold: [0, 0.5, 1]
};

function ChatInitMain(el_name) {
    var chat_ws = new Vue({
        el: el_name,
        template: "#chat",
        data(){
            return{
            onbottom: true, // пользователь просматривает самое новое соощение
            oldest: true, // достигнут конец истории
            updateblock: true, // в данный момент уже происходит подгрузка сообщений из истории
            rooms: [],
            roomtitle: "",
            roomid: "",
            messages: [],
            uid: -1,
            mesinpwidth: 0,
            messendwidth: 50,
            folded: true
            }
        },
        methods: {
            inpwidth(){
                Vue.nextTick(() => {
                    Vue.set(this.$data, 'mesinpwidth', this.$refs["main"].clientWidth - this.messendwidth - 10);
                });
            },
            unfold(){
                Vue.set( this, 'folded', !(this.folded));
            },
            inRoom(){
                return this.roomtitle.length > 0;
            },
            connect(){
                var ws_scheme = window.location.protocol == "https:" ? "wss" : "ws";
                var ws_path;
                if (typeof(chaturl) != 'undefined'){
                    ws_path = ws_scheme + '://' + chaturl + "/chat/stream/";
                } else {
                    ws_path = ws_scheme + '://' + window.location.host + "/chat/stream/";
                };
                console.log("Vue Connecting to " + ws_path);
                this.socket = new WebSocket(ws_path);
                this.socket.onopen = function () {
                    console.log("Connected to chat socket");
                };
                this.socket.onclose = function () {
                    console.log("Disconnected from chat socket");
                };
                this.socket.onmessage = chat_onmessage;
            },
            chatclick:function(id){     
                if (this.inRoom()) { //если мы в чате
                    curroom = this.roomid
                    if(curroom == id){//если мы уже в чате в который собираемся войти то ничего не делаем
                        return;
                    }
                    if(id == -1){//если была нажата ссылка выхода из чата
                        //  покидаем текущий чат
                        $(this).removeClass("joined");
                        this.socket.send(JSON.stringify({ 
                            "command": "leave",  
                            "room": curroom
                        }));
                    }else{
                        // иначе входим в новый чат
                        $(this).removeClass("joined");
                        this.socket.send(JSON.stringify({ 
                            "command": "ljoin",  
                            "from": curroom,
                            "to":id
                        }));
                        $(this).addClass("joined");
                    }
                } else {
                    // если мы не находимся в данный момент в другом чате то просто входим в чат
                    $(this).addClass("joined");
                    this.socket.send(JSON.stringify({
                        "command": "join",
                        "room": id
                    }));
                }
            },
            initobserver(){
                this.observer = new IntersectionObserver(
                    function callback (items) {
                        if(chat_ws.updateblock){//если в данный момент не подгружает
                            for (i=0; i<items.length; i++){
                                if (items[i].isIntersecting){
                                    if (chat_ws.oldest){ //если не достигнут конец истории
                                        if(items[i].target.getAttribute('id') == 'top'){
                                            console.log('intersect top');
                                            Vue.set(chat_ws, 'updateblock', false);
                                            chat_ws.socket.send(JSON.stringify({ 
                                                "command": "getarchive",  
                                                "room": $(".room").attr("id"),
                                                "date": chat_ws.messages[0].date,
                                                "dir":1
                                            }));
                                            console.log(chat_ws.onbottom);
                                        }
                                    }
                                    console.log(items[i].intersectionRatio);
                                    console.log(items[i].isIntersecting);
                                    if(items[i].target.getAttribute('id') == 'bottom'){
                                        if (items[i].intersectionRatio >= 0.5){
                                            Vue.set( chat_ws, 'onbottom', true);
                                        } else {
                                            Vue.set( chat_ws, 'onbottom', false);
                                        }
                                        console.log('intersect bottom 12');
                                        console.log(chat_ws.onbottom);
                                        room = chat_ws.rooms.filter(function(item){
                                            if (item.id == chat_ws.roomid){
                                                Vue.set(item, 'unread', 0);
                                            };
                                        });
                                    }
                                }
                            }
                        };
                    }
                    , config);
                Vue.nextTick (()=>{
                this.observer.observe(this.$refs.top);
                this.observer.observe(this.$refs.bottom);
                });
            },
            delobserver(){
                this.observer.disconnect();
            },
            send(){
                this.socket.send(JSON.stringify({
                    "command": "send",
                    "room": this.roomid,
                    "message": this.$refs.inp.value
                }));
                this.$refs.inp.value = "";
                
            }
        }
    })

    function chat_onmessage(message){
        console.log("Vue Got websocket message " + message.data);
        var data = JSON.parse(message.data);
        // Handle errors
        if (data.error) {
            alert(data.error);
            return;
        }
        else if (data.msg_type == 6){ // новое сообщение в каком-то из доступных чатов
            chat_ws.rooms.filter(function(item){
                if (!((data.room == chat_ws.roomid) && (chat_ws.onbottom))){
                    if (item['id'] == data.room){
                        Vue.set(item, 'unread', 1);
                    }
                }
            });
        }
        else if (data.msg_type == 7){ // новое сообщение было прочитано пользователем в другом окне
            chat_ws.rooms.filter(function(item){
                if (!((data.room == chat_ws.roomid) && (chat_ws.onbottom))){
                    if (item['id'] == data.room){
                        Vue.set(item, 'unread', 0);
                    }
                }
            });
        }
        else if (data.list) { // список чатов
            var rooms = data.rooms;
            for (i in rooms){
                console.log(rooms[i]['name'])
                chat_ws.rooms.push(
                    {
                        'name': rooms[i]["name"], 
                        'id': rooms[i]["id"],
                        'unread': rooms[i]["unread"]
                    }
                );
            };
        }
        // Handle joining
        else if (data.join) {
            console.log("Joining room " + data.join);
            Vue.set(chat_ws, 'oldest', true);
            Vue.set(chat_ws, 'updateblock', true);
            Vue.set(chat_ws, 'roomid', data.join);
            Vue.set(chat_ws, 'roomtitle', data.title);
            chat_ws.initobserver();
        }
        else if (data.leave) {
            console.log("Leaving room " + data.leave);
            Vue.set(chat_ws, 'roomid', '');
            Vue.set(chat_ws, 'roomtitle', '');
            Vue.set(chat_ws, 'messages', []);
            chat_ws.delobserver();
        }
        else if (data.msg_type == -2) {
        if (chat_ws.messages[0].msg_type != -2){
            chat_ws.messages.splice(0, 0,
                {
                    'user': '',
                    'userID': 0, 
                    'date': new Date(0),
                    'message': '',
                    'msg_type': -2,
                    'pos':'',
                    'html': mestype(data) //сообщение в виде html кода
                }
            );
        }
        Vue.set(chat_ws, 'oldest', false);
        Vue.nextTick(function(){// прокручиваем вниз на высоту нового сообщения
            el = document.getElementById('oldest');
            h = el.offsetHeight;
            h += parseInt(window.getComputedStyle(el).getPropertyValue('margin-top'));
            document.getElementById('vroom').scrollBy(0,h);
        });
    } else if (data.message || data.msg_type != 0) {
        var msgdiv = $(".room #vroom");
        if (typeof chat_ws != "undefined"){
            var mesCheck = chat_ws.messages.filter(function(elem){ //если есть сообщение от этого пользователя на это время обновляет его текст вместо дублирования
                if (elem.userID == data.userID && elem.date == data.date){
                    if(elem.message != data.message || elem.user != data.username){
                        Vue.set(elem, 'message', data.message);
                        Vue.set(elem, 'user', data.user);
                        Vue.set(elem, 'html', mestype(data));
                    }
                    return 'dub';
                }
            });
            Vue.set(chat_ws, 'updateblock', true);
            if (mesCheck.length == 0){
                var i = 0;
                var pos = '';
                mesdate = new Date(data.date)
                if(chat_ws.messages.length>0){ //находим индекс первого элемента с датой больше добавляемого
                    while ((i < chat_ws.messages.length) && (new Date(chat_ws.messages[i].date) < mesdate)){
                        i = i + 1;
                    }
                }
                //и вставляем элемент перед ним
                chat_ws.messages.splice(i, 0,
                    {
                        'user': data.username,
                        'userID': data.userID, 
                        'date': data.date,
                        'message': data.message,
                        'msg_type': data.msg_type,
                        'pos':pos,
                        'html': mestype(data) //сообщение в виде html кода
                    }
                );
                if (data.msg_type==-1){ //если сообщение из архива и будет добавлено в начало истории
                    Vue.nextTick(function(){// прокручиваем вниз на высоту нового сообщения 
                        el = document.getElementById(data.date).childNodes[0];
                        h = el.offsetHeight;
                        h += parseInt(window.getComputedStyle(el).getPropertyValue('margin-top'));
                        document.getElementById('vroom').scrollBy(0,h);
                    });
                } else if (chat_ws.onbottom){
                    Vue.nextTick(function(){
                        document.getElementById('vroom').scrollTop = document.getElementById('vroom').scrollHeight;
                    });
                };
            }
        }
        var ok_msg = mestype(data);
    } else {
        console.log(data);
        console.log("Cannot handle message!");
    }

    };

    return chat_ws;
}


