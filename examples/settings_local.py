# -*- coding: utf-8 -*-
#Расположение socket-файла redis, для доступа через адрес и порт оставить пустым
REDIS_SOCKET_DIR = ''
REDIS_HOST = ''
REDIS_PORT = ''
#номер БД редиса для остальных функций (контроль онлайна)
REDIS_DB = 1
#номер БД редиса для channels
REDIS_CHANNELS_DB = 0
REDIS_MESSAGE_EXPIRE_TIME = 0
if (len(REDIS_SOCKET_DIR)==0):
    BROKER_URL = 'redis://' + REDIS_HOST + ':' + REDIS_PORT + '/'+str(REDIS_CHANNELS_DB)
    CELERY_RESULT_BACKEND = 'redis://' + REDIS_HOST + ':' + REDIS_PORT + '/'+str(REDIS_CHANNELS_DB)
else:
    BROKER_URL = 'unix://' + REDIS_SOCKET_DIR + '/'+str(REDIS_CHANNELS_DB)
    CELERY_RESULT_BACKEND = 'unix://' + REDIS_SOCKET_DIR + '/'+str(REDIS_CHANNELS_DB)
# БД чата
DB_NAME=''
DB_USER=''
DB_PASS=''
DB_HOST=''
DB_PORT=''

# IP СУРа (для добавления Cross-Origin заголовков и отмены проверки CSRF)
SUR_HOSTS=[]

# БД СУРа
SUR_DB_NAME=''
SUR_DB_USER=''
SUR_DB_PASS=''
SUR_DB_HOST=''
SUR_DB_PORT=''

#Использовать БД СУРа (для авторизации и имён пользователей)
SUR_AUTH=True

# SECRET_KEY чата
SECRET_KEY=''

# SECRET_KEY СУРа для расшифровки данных сессии
SUR_SECRET_KEY=''