/*
	Проба использования npm для конфигурирования WebPack для Django
*/

/*  
В webpack.config.js определить:
	const django_conf = {
	  apps: [... ],
	  dir_name: 'vues',  //~ Нужно задавать, если нужен другой
	  static_subdir: 'js',  //~ или '' если не нужен
	  app_basedir: "", //~ Каталог, добавляемый к application, если их имя начинается с '~'
	  static_first: false // если true, то результат в /static/appname, иначе /appname/static/apname
	  ext_convert: {'ext':'new_ext'} //Конвертация расширения.
	}
*/

var fs = require('fs');
var path = require('path');
var Datastore = require('nedb')
  , db = new Datastore({ filename: './static/chunkdata.json', autoload: true });

function get_files(django_conf, iapp_name) {
	// Список файлов
	var base = "", app_name=iapp_name;
	if (app_name[0]=="~") {
		base = django_conf.app_basedir;
		app_name = iapp_name.slice(1);
	}
	let pth = path.join(
		base,
		app_name, 
		django_conf.dir_name,
		app_name,);

	let res = [];
	let files = fs.readdirSync(pth);
	for (let j=0, ll = files.length; j<ll; j++) {
		let fst = fs.statSync(path.join(pth, files[j]));
		if (!fst.isDirectory()) {
			let pfn = path.parse(files[j])
			res.push({name: pfn.name, ext: pfn.ext, base: pfn.base})
		}
	}
	return res
}

function entries (django_conf) {
	// Возвращает список entry по всем apps
	var res_ent = {}, res_mod = [], res_alis={}, conf_alias = django_conf.aliases;
	if (conf_alias === undefined) conf_alias = {};
	res_mod.push('node_modules');
	for (let i=0, l=django_conf.apps.length; i<l; i++) {
		let files = get_files(django_conf, django_conf.apps[i]), 
			apps = django_conf.apps[i], base = "";
		if (apps[0]=="~") {
			base = django_conf.app_basedir;
			apps = apps.slice(1);
		}
		for (let j=0, ll = files.length; j<ll; j++) {
			res_ent[apps + "." + files[j].base] = "./" + path.join(
				base,
				apps, 
				django_conf.dir_name, 
				apps,
				files[j].base)
		}
		res_mod.push(path.resolve(path.join(base, apps, django_conf.dir_name)));
		let app_key = conf_alias[apps];
		if (app_key === undefined) app_key = apps; 
		res_alis[app_key] = path.resolve(path.join(base, apps, django_conf.dir_name, apps));
	}
	console.log("aliases", res_alis);
	return {entries: res_ent, modules: res_mod, aliases: res_alis}
}

const filename_func = (django_conf) => (chunkData) => {
  var name = chunkData.chunk.name, fname = 'bundle';
  var p1 = name.indexOf(".");
  let hash = chunkData.chunk.hash.slice(0,6);
  if (p1 == -1) {
	return path.join(name)
  } else {
	let appl = name.substring(0, p1), base = name.substring(p1 + 1), hbase=null;
	let p_ex = base.lastIndexOf(".");
	if (p_ex>0) {
		//let ext = base.slice(p_ex+1);
		let cur_ext = base.slice(p_ex+1);
		let new_ext = django_conf.ext_convert[cur_ext];
		if (typeof new_ext !== "undefined") {
			cur_ext = new_ext
		}
		base = `${base.slice(0, p_ex)}.${cur_ext}`
		hbase = `${base.slice(0, p_ex)}.${hash}.${cur_ext}`
	}

	//db.insert({appl, base, chunk_name:chunkData.chunk.name, hash:chunkData.chunk.hash}, function (err, newDoc) {   // Callback is optional
	//});
	db.update(
		{chunk_name: chunkData.chunk.name,},
		{appl, base, chunk_name: chunkData.chunk.name, 
			lhash: chunkData.chunk.hash,
			hash,
			hbase,
			renderedHash: chunkData.chunk.renderedHash,
			path: path.join(appl, django_conf.static_subdir, base),
		},
		{upsert: true, multi: true},
		);

	if (django_conf.static_first) {
		return path.join(appl, django_conf.static_subdir, base)
	} else {
		return path.join(appl, 'static', appl, django_conf.static_subdir, base)
	}
  }
};

function normalize_conf(django_conf) {
	if (typeof django_conf.apps == "undefined") {
		throw "'apps' setting not present, but is required."
	}
	if (typeof django_conf.dir_name == "undefined") {
		django_conf.dir_name = "vues"
	}
	if (typeof django_conf.static_subdir == "undefined") {
		django_conf.static_subdir = ""
	}
	if (typeof django_conf.app_basedir == "undefined") {
		django_conf.app_basedir = ""
	}
	if (typeof django_conf.static_first == "undefined") {
		django_conf.static_first = false
	}
	if (typeof django_conf.ext_convert == "undefined") {
		django_conf.ext_convert = {}
	}
}

module.exports = {
	setup_webpack(wp_conf, django_conf) {
		var e;
		normalize_conf(django_conf);
		e = entries(django_conf);
		wp_conf.entry = e.entries;
		if (typeof wp_conf.output == "undefined") {
			wp_conf.output = {}
		}
		wp_conf.output.filename = filename_func(django_conf);
		wp_conf.output.libraryTarget = 'window';
		if (typeof wp_conf.resolve == "undefined") {
			wp_conf.resolve = {}
		}
		// wp_conf.resolve.modules = e.modules;
		let alias = wp_conf.resolve.alias;
		if (alias === undefined || alias === null) {
			alias = {}
		};
		wp_conf.resolve.alias = Object.assign(alias, e.aliases);
		// console.log("wp_conf.resolve", wp_conf.resolve);
	}
}

