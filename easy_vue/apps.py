# -*- coding: utf-8 -*-

from django.apps import AppConfig


class EasyVueConfig(AppConfig):
    name = 'easy_vue'
