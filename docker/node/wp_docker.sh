#!/bin/bash
comm=$1
shift
case "$comm" in
	start) # Init project, copy node_modules
		#cp -a /tmp/node_modules /usr/src/app/
		echo "Prepare node_modules"
		cd "/code/"
		#rm -f -R ./node_modules
        #ls /tmp/proj/node_modules
		cp /tmp/proj/package.json ./package.json
		cp /tmp/proj/package-lock.json ./package-lock.json
		cp -R /tmp/proj/node_modules/ ./node_modules/
        # если сразу скопировать все модули в node_modules оно не работает
        cp -R ./node_modules/node_modules/ .
		echo "node_modules ready now";
	;;
	node) # Run node command
		echo "Run node command"
		cd "/code/" 
		/usr/local/bin/docker-entrypoint.sh node "$@"
	;;
	npm) # Run npm for project
		echo "Run npm command"
		cd "/code/" 
		/usr/local/bin/docker-entrypoint.sh npm "$@"
	;;
	sh) 
		echo "Execute any bash command"
		cd "/code/"
		/bin/sh -c "$*"
	;;
	*) 
		echo "No command specified" 
		echo "Use: start | npm | node | sh" 
	;;
esac