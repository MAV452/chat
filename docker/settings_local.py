# -*- coding: utf-8 -*-

import os

#Расположение socket-файла redis, для доступа через адрес и порт оставить пустым
REDIS_SOCKET_DIR = ''
REDIS_HOST = 'redis'
REDIS_PORT = '6379'
#номер БД редиса для остальных функций (контроль онлайна)
REDIS_DB = 1
#номер БД редиса для channels
REDIS_CHANNELS_DB = 0
REDIS_MESSAGE_EXPIRE_TIME = 0
if (len(REDIS_SOCKET_DIR)==0):
    BROKER_URL = 'redis://' + REDIS_HOST + ':' + REDIS_PORT + '/'+str(REDIS_CHANNELS_DB)
    CELERY_RESULT_BACKEND = 'redis://' + REDIS_HOST + ':' + REDIS_PORT + '/'+str(REDIS_CHANNELS_DB)
else:
    BROKER_URL = 'unix://' + REDIS_SOCKET_DIR + '/'+str(REDIS_CHANNELS_DB)
    CELERY_RESULT_BACKEND = 'unix://' + REDIS_SOCKET_DIR + '/'+str(REDIS_CHANNELS_DB)
# БД чата
DB_NAME='db_chat'
DB_USER=os.environ.get('DB_USER','')
DB_PASS=os.environ.get('DB_PASS','')
DB_HOST='db_chat_serv'
DB_PORT='5432'

# IP СУРа (для добавления Cross-Origin заголовков и отмены проверки CSRF)
SUR_HOSTS=os.environ.get('SUR_URLS','').split(';')

SUR_DB_NAME=os.environ.get('SUR_DB_NAME','')
SUR_DB_USER=os.environ.get('SUR_DB_USER','')
SUR_DB_PASS=os.environ.get('SUR_DB_PASS','')
SUR_DB_HOST='db_sur'
SUR_DB_PORT=os.environ.get('SUR_DB_PORT','5432')

#Использовать БД СУРа (для авторизации и имён пользователей)
SUR_AUTH=os.environ.get('SUR_AUTH','False').lower() in ('true', '1', 't')

# SECRET_KEY чата
SECRET_KEY='1afkja^tz1bci9@290ny_ua3un9+tgrf_2k78!71xhb^ppd6-%'

# SECRET_KEY СУРа для расшифровки данных сессии
SUR_SECRET_KEY=''