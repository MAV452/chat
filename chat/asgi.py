import os
from django.core.asgi import get_asgi_application

# Fetch Django ASGI application early to ensure AppRegistry is populated
# before importing consumers and AuthMiddlewareStack that may import ORM
# models.
import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "chat.settings")
django.setup()
from chatapp.a_consumers import ChatConsumer

from django.conf.urls import url

from channels.routing import ProtocolTypeRouter, URLRouter
from channels.auth import AuthMiddlewareStack
from channels.sessions import SessionMiddlewareStack

application = (ProtocolTypeRouter({
    # Django's ASGI application to handle traditional HTTP requests
    "http": get_asgi_application(),

    # WebSocket chat handler
    'websocket': SessionMiddlewareStack(AuthMiddlewareStack(
        URLRouter([
            url(r"^chat/stream/$",ChatConsumer.as_asgi()),
        ])),
    ),
}))