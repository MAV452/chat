��    J      l  e   �      P     Q  	   _     i     r     x  ]   �  ]   �     B  
   R     ]     b     y     �     �     �     �     �     �     �     �     �     �               "     2     @     E     Y  9   j     �     �  
   �  
   �     �  	   �     �     �     	     	     ,	  
   <	  
   G	     R	     `	     m	  
   y	     �	     �	     �	     �	     �	  
   �	     �	     �	  	   �	     
     
     !
     <
     L
     Q
     i
     m
     {
     �
     �
     �
     �
     �
     �
     �
     �
  �  �
     �     �  )   �     �       �   2  �   �  %   j  #   �  
   �  J   �  U   
  "   `     �     �     �     �  !   �     �               8     R  /   l     �  !   �  
   �  /   �  *     |   B     �  0   �     �       $   5     Z     t     �  #   �  G   �  !        )     G     e     �     �      �     �  ,   �          9  '   X     �  1   �  )   �     �  $        ;  9   Q     �     �  A   �          	     )     6     G  &   L     s  
   �  &   �     �     �        <           $       0                          8   G   ?      B      !         /      
      "   H   	   I       +       5   >       )   (                  ,   E      :   J   .       =       1      7   F   9   -      @      A                                     *   &   6   %   2              C               3                        4       D   ;       '          #    Access denied Add files Add user Admin Already added An error occured while trying to play a audio. You can download audio directly via link below An error occured while trying to play a video. You can download video directly via link below Attaching files Auth error Blue Can't fetch max length Can't remove the only admin Change your role Chat Color themes Create Create room Create rooms Current Default Delete room Download audio Download video Editing message End of messages Enter message Exit Exit on all devices File check error File too large, total size of all files must be less then Hide Internal Server Error Lang error Leave room Leave this room Load more Log out? Login failed Max length is Max name length exceeded Message deleted Name error Name taken Network Error New messages No messages No results No rooms No such user Permission denied Remove all files Remove user Room error Search by message text Select user Set theme Start entering name Theme error Too many files, maximum is Uploading files User User is not in the room You audio sources cancel edited from joined the room left the room login searchbar error stop all to Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Доступ запрещён Добавить файлы Добавить пользователя Администратор Уже добавлен При загрузке аудио произошла ошибка. Вы можете скачать аудио напрямую по ссылке ниже При загрузке видео произошла ошибка. Вы можете скачать видео напрямую по ссылке ниже Прикрепление файлов Ошибка авторизации Синий Не удалось получить максимвальную длину Нельзя удаллить единственного администратора Изменить вашу роль Чат Цветовые темы Создать Создать комнату Создавать комнаты Текущий По умолчанию Удалить комнату Скачать аудио Скачать видео Редактирование сообщения Конец сообщений Введите сообщение Выход Выйти на всех устройствах Ошибка проверки файлов Слишком большой размер, общий размер всех файлов должен быть меньше Скрыть Внутренняя ошибка сервера Ошибка языка Покинуть комнату Покнуть эту комнату Загрузить ещё Выйти? Вход не удался Максимальная длина Превышена максимальная длина названия Сообщение удалено Ошибка названия Название занято Нет соединения Новые сообщения Нет сообщений Ничего не найдено Нет комнат Нет такого пользователя Доступ запрещён Убрать все файлы Удалить пользователя Ошибка комнаты Искать по тексту сообщения Выберете пользователя Установить тему Начните вводить ФИО Ошибка темы Слишком много файлов, максимум: Отправка файлов Пользователь Пользователь не добавлен  в комнату Вы источников аудио отмена изменено от присоеденился к чату покинул чат Войти ошибка строки поиска остановить всё до 